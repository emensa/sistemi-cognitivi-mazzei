\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Stochastic PoS Tagger}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Consegna}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Implementazione}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Sperimentazione}{2}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Risultati}{3}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Valutazione dei risultati}{5}{subsection.1.3.2}
\contentsline {paragraph}{Viterbi vs Baseline.}{5}{section*.2}
\contentsline {paragraph}{L'importanza del tagset.}{5}{section*.3}
\contentsline {paragraph}{In merito alla normalizzazione.}{5}{section*.4}
\contentsline {paragraph}{In merito allo smoothing.}{5}{section*.5}
\contentsline {paragraph}{Sugli errori pi\IeC {\`u} comuni.}{6}{section*.6}
\contentsline {chapter}{\numberline {2}Parsing di PCFG e algoritmo CKY probabilistico}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Consegna (parte A)}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Implementazione (parte A)}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Pulizia del corpus}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Problematiche riscontrate}{9}{subsection.2.2.2}
\contentsline {paragraph}{Regole non in Chomsky Normal Form.}{9}{section*.8}
\contentsline {paragraph}{La complessit\IeC {\`a}.}{9}{section*.9}
\contentsline {subsection}{\numberline {2.2.3}Lo smoothing}{10}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Sperimentazione (parte A)}{10}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Risultati}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Valutazione dei risultati}{11}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}PoS Tagger + CKY (parte B)}{11}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Modifiche apportate}{11}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Risultati}{12}{subsection.2.4.2}
\contentsline {subsubsection}{Valutazione dei risultati}{12}{section*.10}
\contentsline {chapter}{\numberline {3}Sistema di Transfer Machine Translation}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Consegna}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Implementazione}{13}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Le regole di traduzione}{13}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Sperimentazione}{14}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Risultati}{14}{subsection.3.3.1}
\contentsline {subsubsection}{Valutazione dei risultati}{14}{section*.11}
