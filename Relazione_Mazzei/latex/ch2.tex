%!TEX root = main.tex

\chapter{Parsing di PCFG e algoritmo CKY probabilistico}
\thispagestyle{empty}

\section{Consegna (parte A)}
Estrazione di una grammatica PCFG a partire dal Turin University Treebank\footnote{\url{http://www.di.unito.it/~tutreeb/}.}, training ed esecuzione dell'algoritmo CKY probabilistico e valutazione dei risultati.

\section{Implementazione (parte A)}
L'implementazione è sia in Java che in Python con l'ausilio della libreria \texttt{NTLK}\footnote{\url{http://www.nltk.org/}.} per il trattamento di grammatiche probabilistiche. \\[2ex]
Il workflow svolto dal sistema è descritto dallo schema in Figura~\ref{fig:cky_schema}.
\begin{figure}[ht!]
	\centering
	\includegraphics[scale=0.55]{img/cky_schema}
	\caption{Schema dei moduli e del workflow del sistema.}
	\label{fig:cky_schema}
\end{figure}
Le classi e gli script fondamentali coinvolti sono i seguenti:
\begin{itemize}
	 \item \texttt{elaborateTUT.py}, che compie diversi task. Una volta preso in input il corpus (\texttt{tut.penn}) lo ripulisce e produce quattro file fondamentali:
	 \begin{itemize}
	 \item \texttt{tut-train.penn} che contiene il 90\% del corpus (pulito). 
	 \item \texttt{tut-test.penn} che contiene il 10\% del corpus (pulito).
	 \item \texttt{tut-grammar.pcfg} che contiene la grammatica probabilistica estratta grazie alle funzioni di NTLK.
	 \item \texttt{tut-test-CNFtree.penn} che contiene il test set in forma CNF (necessario per \texttt{evalb}).
	 \item \texttt{tut-test-flatten.penn} che contiene il test set in forma testuale, input per l'algoritmo di CKY.
	 \end{itemize}
	 \item \texttt{TUTUtil.java}, che implementa la creazione della grammatica a partire dal file \texttt{tut-grammar.pcfg}. Ulteriore compito di questa classe è l'estrazione del test set dal file \texttt{tut-test-flatten.penn}. Infine, contiene il metodo \texttt{elaborateTUT\_py()} che si occupa eseguire il nostro script in Python.
	 \item Le classi \texttt{PGrammar.java}, \texttt{PProduction.java},  \texttt{Production.java}, \\ \texttt{Symbol.java}, \texttt{Terminal.java} e \texttt{Nonterminal.java} che permettono la rappresentazione della grammatica.
	 \item \texttt{EvaluateUtil.java}, che si occupa di lanciare \texttt{evalb}.
	 \item \texttt{CKYParser.java}, che implementa CKY sfruttando la grammatica generata precedentemente. 
	 \item \texttt{MorphitWrapper.java}, la classe singleton già usata nell'esercizio precedente. È stato aggiunto il metodo \texttt{smoothingCKY(word)} il quale fornisce una lista di coppie GooglePoS-Probabilità relative alla parola passata in input. Il metodo \texttt{smoothedProductionsForWord(word)} in \texttt{TUTUtil.java} si occuperà poi di produrre una lista di possibili produzioni per quella parola.
\end{itemize}

\subsection{Pulizia del corpus}
La pulizia del corpus si è concentrata sulla:
\begin{itemize}
	\item Rimozione delle annotazioni ridondanti.
	\item Rimozione di caratteri illeggibili/non significativi. 
	\item Aggiunta di un tag iniziale, chiamato \textcc{H}, che fungesse da contenitore delle sentences.
\end{itemize}

\subsection{Problematiche riscontrate}
Durante l'implementazione abbiamo riscontrato due problematiche importanti, di cui parliamo brevemente nel seguito.

\paragraph{Regole non in Chomsky Normal Form.} Il metodo fornito dalle librerie NTLK \texttt{chomsky\_normal\_form()}, trasforma un albero nella sua versione normalizzata. Tale passaggio è fondamentale poiché l'algoritmo CKY si fonda sull'assunzione che vi siano soltanto regole del tipo \textcc{A $\to$ BC} con \textcc{B} e \textcc{C} simboli non terminali, oppure \textcc{A $\to$ a} con \textcc{a} simbolo terminale. \\[2ex]
La problematica nasce dal fatto che sia il metodo \texttt{chomsky\_normal\_form()} che il metodo \texttt{collapse\_unary()} \emph{non} accorpino le regole che hanno come testa lo start symbol della grammatica (\textcc{H}, nel nostro caso). Ciò ha portato all'esistenza delle seguenti regole all'interno della nostra grammatica probabilistica:
\begin{lstlisting}[style=javabox, label=listing:LABEL, caption=Regole non in CNF prodotte da NTLK.]
	H -> S [0.712551]
	H -> PRN [0.00202429]
	H -> PP [0.0212551]
	H -> NP [0.262146]
	H -> S+VP [0.00101215]
	H -> ADVP [0.00101215]
\end{lstlisting}
Per ovviare a questo problema, al termine dell'algoritmo CKY andiamo ad esaminare i non teminali che si trovano nella cella in alto a destra della tabella al fine di verificare se qualcuno di essi matcha con il corpo delle regole che abbiamo appena visto. Laddove possibile, la scelta della produzione avverrà in maniera analoga a quanto calcolato durante l'algoritmo.

\paragraph{La complessità.} Come già studiato in teoria, l'algoritmo CKY ha complessità $O(n^5)$ nel caso medio. Questo fatto si rende evidente quando l'algoritmo deve affrontare frasi particolarmente lunghe: un'intera esecuzione sul test set ha richiesto più di 20 minuti. \\[2ex]
Per migliorare le prestazioni dell'algoritmo è stato sufficiente costruire all'interno della classe \emph{PGrammar} una HashMap inversa che collegasse ad ogni set di \emph{Symbol} un set di produzioni che le coinvolgessero.

\subsection{Lo smoothing}
Anche CKY ha richiesto l'impiego di smoothing. In questo caso si è riutilizzato il codice della classe \texttt{MorphitWrapper.java}, già in grado di fornire le probabilità per una coppia GooglePoS-Parola. Giacché i tag del TUT sono diversi da quelli di Google ed in generale i primi sono più dettagliati dei secondi\footnote{Più precisamente si ha che la trasformazione in CNF produce diversi tag per lo stesso TUTPoS.}, si è deciso di adottare il seguente sistema di mapping:
\begin{enumerate}
	\item Data una parola $w$ recuperiamo tutti i PoS tag di Morph-it! (debitamente tradotti in GooglePoS) ad essa assegnati. A partire da essi si calcolano le probabilità per ogni coppia GooglePoS-Parola seguendo l'approccio già riportato nel capitolo precedente.
	\item Giacché in generale ad un tag GooglePoS $g$ corrispondono $\{t_1, \dots, t_n\}$ TUTPoS, la probabilità $p$ assegnata alla coppia $(g, w)$ viene uniformente distribuita sugli $n$ TUTPoS. Si produranno pertanto una serie del produzioni del tipo:
	\begin{equation*}
		\begin{split}
			t_1 &\to w~~ \Bigg[\frac{p}{n}\Bigg]\\
			\vdots\\
			t_n &\to w ~~\Bigg[\frac{p}{n}\Bigg]\\			
		\end{split}
	\end{equation*}
	Si ponga attenzione al fatto che i tag $\{t_1, \dots, t_n\}$ \emph{non} sono soltanto quelli di TUT, ma sono quelli recuperati dalla forma CNF del nostro test set. Evidentemente, ciò implica che la traduzione GooglePoS-TUTPoS sia stata fatta manualmente. \\[2ex]
	A questo punto le produzioni vengono trattate come se fossero sempre state presenti nella grammatica.
\end{enumerate}

\section{Sperimentazione (parte A)}
\subsection{Risultati}
La nostra sperimentazione ha portato ai seguenti risultati, calcolati con l'ausilio della libreria \texttt{evalb}\footnote{\url{http://nlp.cs.nyu.edu/evalb/}.}:
\begin{center}
  \begin{tabular}{| c | c |}
    \hline                        
     Campo & Valore  \\ \hline \hline
	Number of sentence        &    108 \\ \hline 
	Number of Error sentence  &     11 \\ \hline
	Number of Skip  sentence  &      0 \\ \hline
	Number of Valid sentence  &     97 \\ \hline
	Bracketing Recall         &  55,14 \\ \hline
	Bracketing Precision      &  55,14 \\ \hline
	Bracketing FMeasure       &  55,14 \\ \hline
	Complete match            &   5,15 \\ \hline
	Average crossing          &   6,34 \\ \hline
	No crossing               &  29,90 \\ \hline
	2 or less crossing        &  42,27 \\ \hline
	Tagging accuracy          &  88,78 \\ \hline
	Execution time          &  61,23 sec \\ \hline	
  \end{tabular}
\end{center}

\subsection{Valutazione dei risultati}
Possiamo osservare delle performances abbastanza buone. Vi sono soltanto 11 su 108 frasi erroneamente parsificate. In generale la tag accurancy è soddisfacente. \\[2ex]
Riteniamo che tali performances siano rese possibili anche grazie all'ausilio di Morph-it! in fase di smoothing.

\section{PoS Tagger + CKY (parte B)}

\subsection{Modifiche apportate}
L'integrazione del PoS Tagger e di CKY ha comportato la modifica di alcune parti del nostro codice. Le elenchiamo nel seguito.
\begin{enumerate}
	\item Lo script di Python \texttt{elaborateTUT.py} implementa la sostituzione di tutti TUTPoS in GooglePoS; per indicare ciò, tutti i file generati dallo script riportano la dicitura \emph{4PoS}. Inoltre, \texttt{elaborateTUT.py} produce anche un nuovo file \texttt{tut-clean-train-CNFcolumns\_4PoS.penn}, che contiene il training set TUT nella stessa forma `'a colonne`' del training set UDTP.\\[2ex] Grazie a questa accortezza il codice sviluppato per estrapolare le probabilità di emission e di transition nell'esercizio del PoS Tagging può essere riutilizzato anche su questo training set.
	\item La grammatica generata nel file \texttt{tut-grammar\_4PoS.pcfg} non contiene produzioni lessicalizzate. 
	\item Le produzioni lessicali vengono elaborate da Java sfruttando i risultati dell'algoritmo di Viterbi. Più precisamente, il PoS Tagger viene allenato sul file \texttt{tut-clean-train-CNFcolumns\_4PoS.penn} e poi eseguito sul test set\footnote{Il file è lo stesso sia per CKY che per Viterbi: \texttt{tut-test-flatten.penn}.}. Si ottiene così una serie di coppie Parola-Tag, ognuna delle quali sottointende una produzione lessicalizzata \textcc{Tag $\to$ Parola}. A questo punto, per ogni produzione si contano le sue $c$ occorrenze e se ne ottiene la probabilità dividendo $c$ per le occorrenze della testa della produzione.
\end{enumerate}

\subsection{Risultati}
Sempre usando la libreria \texttt{evalb} abbiamo ottenuto i seguenti risultati:
\begin{center}
  \begin{tabular}{| c | c |}
    \hline                        
     Campo & Valore  \\ \hline \hline
	Number of sentence        &    108 \\ \hline 
	Number of Error sentence  &     32 \\ \hline
	Number of Skip  sentence  &      0 \\ \hline
	Number of Valid sentence  &     76 \\ \hline
	Bracketing Recall         &  39,30 \\ \hline
	Bracketing Precision      &  39,30 \\ \hline
	Bracketing FMeasure       &  39,30 \\ \hline
	Complete match            &   5,26 \\ \hline
	Average crossing          &   10,12 \\ \hline
	No crossing               &  7,89 \\ \hline
	2 or less crossing        &  25,00 \\ \hline
	Tagging accuracy          &  82,71 \\ \hline
	Execution time          &  19,79 sec \\ \hline	
  \end{tabular}
\end{center}

\subsubsection{Valutazione dei risultati}
Possiamo osservare come il numero di frasi errate sia aumentato rispetto al caso in cui non si utilizzi un PoS Tagger esterno. Questo fatto è giustificabile per due motivi:
\begin{enumerate}
	\item Avendo noi ridotto il numero di tag (i GooglePoS sono meno dettagliati dei TUTPoS), le produzioni generate risultano essere più generali ma meno precise (in termini di probabilità). Questo porta, in fase di esecuzione di CKY, ad una maggiore chance di errore.
	\item Utilizzare regole lessicalizzate ottenute da un PoS Tagger significa non dover usare smoothing: questo dovrebbe teoricamente portare a dei vantaggi, che però sono poco significativi poiché lo smoothing della parte A, implementato con Morph-it!, pare già fornire ottimi soddifacenti.
\end{enumerate}





