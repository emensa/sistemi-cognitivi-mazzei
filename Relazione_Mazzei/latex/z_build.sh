#!/bin/bash

# Come parametro gli viene passata la folder attuale
totalpath="$1$2$3"

IFS='/' read -ra FPATH <<< "$totalpath"

path_length=${#FPATH[@]}
CORSO="${FPATH[@]:$path_length-2:1}"


# CORSO contiene il corso, che è anche la cartella dove si trova il progetto
pdf_filename="${CORSO}.pdf"

# LA COPIA AVVIENE DA QUI IN POI
cp main.pdf ../"$pdf_filename" 
