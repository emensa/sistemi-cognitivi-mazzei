%!TEX root = main.tex

\chapter{Stochastic PoS Tagger}
\thispagestyle{empty}

\section{Consegna}
Creazione di un PoS Tagger statistico basato su Hidden Markov Model per la lingua italiana, utilizzando il corpus `'The Universal Dependecy Treebank Project`'\footnote{\url{https://uni-dep-tb.googlecode.com/svn/trunk/universal_treebanks_v2.0.tar.gz}.}.

\section{Implementazione}
L'implementazione è in Java con l'ausilio della libreria \texttt{guava}\footnote{\url{https://code.google.com/p/guava-libraries/}.} per l'utilizzo di HashMap con chiavi duplicate. \\[2ex]
Le classi fondamentali coinvolte sono le seguenti:
\begin{itemize}
	\item \texttt{FileUtil.java}, che contiene tutti i metodi di estrazione dal corpus sia per il training set che per il test set. Fra i metodi di estrazione citiamo \texttt{UDTP\_TransitionEmissionProbability}, che effettua il calcolo delle due matrici di \emph{transition} ed \emph{emission}.
	\item \texttt{EvaluateUtil.java}, che implementa metodi di valutazione del risultato del tagger rispetto al test set. I risultati vengono stampati su file.
	\item \texttt{BaselinePoSTagger.java}, che implementa il più banale algoritmo di classificazione: assegnare ad ogni parola il suo tag più frequente nel corpus, e, qualora la parola esaminata non fosse presente nel corpus, assegnare il tag \textcc{NOUN}.
	\item \texttt{ViterbiPoSTagger.java}, che implementa l'algoritmo di Viterbi, il quale trova la migliore sequenza di stati in una sequenza di eventi osservati in un processo markoviano. Viterbi è un algoritmo di programmazione dinamica che è necessario per domare l'esplosione combinatoria altrimenti inevitabile.
	\item \texttt{MorphitWrapper.java}, una classe singleton che al suo interno mantiene un'istanza dell'oggetto \texttt{MORPHIT}, ottenuto parsificando la versione testuale di Morph-it!\footnote{\url{http://sslmitdev-online.sslmit.unibo.it/linguistics/morph-it.php}.}. Degno di nota il metodo \texttt{smoothingViterbi(tag, word, ntag)} che restituisce una probabilità di emissione per la coppia Tag-Word fornita come parametro calcolando statistiche sul corpus di Morph-it!.
\end{itemize}

\section{Sperimentazione}
Al fine di testare il nostro algoritmo abbiamo implementato diverse soluzioni e ne abbiamo testate alcune combinazioni. Ci siamo concentrati sui seguenti parametri:
\begin{enumerate}
	\item Scelta del tagset: colonna 4 o colonna 5 dell'UDTP.
	\item Normalizzazione: 
	\begin{itemize}
		\item \emph{None}: nessuna normalizzazione.
		\item \emph{Capitalize}: tutte le parole sono rese minuscole.
		\item \emph{Lemming}: tutte le parole sono sostituite dal loro lemma (forma normale) grazie all'ausilio di Morph-it!.
	\end{itemize}
	\item Smoothing: 
	\begin{itemize}
		\item \emph{Constant}: adottabile nell'algoritmo di baseline, il tag restituito è sempre \textcc{NOUN}.
		\item \emph{Unknown}: adottabile nell'algoritmo di Viterbi, la probabilità restituita è $\frac{1}{\texttt{\#TAG}}$.
		\item \emph{Morphit}: adottabile nell'algoritmo di Viterbi, effettua un calcolo piuttosto sofististicato per restituire una probabilità che rispecchi il contenuto di Morph-it!. Volendo calcolare la probabilità di emissione $P(w | t)$, si individuano i tag ${t_1, t_2, \ldots, t_n}$ assegnati a $w$ all'interno di Morph-it! e per ognuno di questi viene calcolata la formula:
		\begin{equation*}
		 	P_{t_i} = \frac{C(t_i,w)}{C(w)} = \frac{C(t_i,w)}{n}
		 \end{equation*}
		A questo punto, se il tag $t$ fornito in input è presente con indice $j$ fra i ${t_1, t_2, \ldots, t_n}$ si restituisce $P_{t_j}$, in caso contrario ci si comporta in maniera analoga all'\emph{Unknown} smoothing.\\[2ex]
		Qualora $w$ non sia presente in Morph-it! si vanno a testare alcune fini condizioni mediante le quali si cerca di attribuire comunque una probabilità sensata. In particolare:
		\begin{itemize}
			\item Si verifica se $w$ è un numero ed in tal caso si assegna probabilità 1 se $t = \textcc{NUM}$ e 0 altrimenti.
			\item Si verifica se $t = \textcc{NOUN}$: in tal caso, seguendo il principio sul quale si basa il \emph{Constant} smoothing, attribuiamo una probabilità di:
			\begin{equation*}
				\frac{\texttt{\#TAG}-1}{\texttt{\#TAG}}	
			\end{equation*}
			mentre, per tutti gli altri tag attribuiamo una probabilità di:
			\begin{equation*}
				 \frac{1}{\texttt{\#TAG}\cdot(\texttt{\#TAG}-1)}	
			 \end{equation*}
		\end{itemize}
		L'introduzione di queste regole potrebbe far lontanamente pensare ad un approccio ibrido probabilità-regole.
	\end{itemize}
	
\end{enumerate}

\subsection{Risultati}
Nella Tabelle~\ref{tab:pos_results_1} e~\ref{tab:pos_results_2} osserviamo i risultati della sperimentazione che abbiamo effettuato\footnote{I test sono stati eseguiti su iMac 2,7 GHz Intel Core i5, 8GB RAM.}. La colonna MNRT indica il \emph{Most Not Recognized Tag}, mentre la colonna MMC indica la \emph{Most Mismatched Couple} (il primo elemento è quello del corpus il secondo è la risposta del tagger).
\begin{center}
  \begin{tabular}{| c | c | c | c | c | c |}
    \hline                        
     Tagset & Parametri & Accuracy  & Tempo & MNRT& MMC  \\ \hline \hline
    C4 & \makecell[l]{Norm: None\\ Smoot: Constant} & $92,10\%$ & 0,014 s  &  \makecell{ \textcc{VERB} \\ $35,68\%$ } & \makecell{ \textcc{VERB}-\textcc{NOUN} \\ $34,58\%$ }\\ \hline
    C4 & \makecell[l]{Norm: Capitalize\\ Smoot: Constant} & $92,15\%$ & 0,009 s  &  \makecell{ \textcc{VERB} \\ $33,66\%$ } & \makecell{ \textcc{VERB}-\textcc{NOUN} \\ $32,55\%$ }\\ \hline
    C4 & \makecell[l]{Norm: Lemming\\ Smoot: Constant} & \underline{$92,78\%$} & 0,01 s  &  \makecell{ \textcc{NOUN} \\ $22,44\%$ } & \makecell{ \textcc{VERB}-\textcc{NOUN} \\ $18,38\%$ }\\ \hline\hline
    C5 & \makecell[l]{Norm: None\\ Smoot: Constant} & $89,25\%$ & 0,014 s  &  \makecell{ \textcc{VERB} \\ $33,10\%$ } & \makecell{ \textcc{VERB}-\textcc{NOUN} \\ $25,31\%$ }\\ \hline
    C5 & \makecell[l]{Norm: Capitalize\\ Smoot: Constant} & $89,14\%$ & 0,007 s  &  \makecell{ \textcc{VERB} \\ $31,17\%$ } & \makecell{ \textcc{VERB}-\textcc{NOUN} \\ $23,25\%$ }\\ \hline
    C5 & \makecell[l]{Norm: Lemming\\ Smoot: Constant} & \underline{$89,72\%$} & 0,009 s  &  \makecell{ \textcc{VERB} \\ $29,32\%$ } & \makecell{ \textcc{VERB}-\textcc{AUX} \\ $14,40\%$ }\\ \hline
  \end{tabular}
    \captionof{table}{Risultati della sperimentazione usando il tagger Baseline.}     
    \label{tab:pos_results_1}      
\end{center}

\begin{center}
  \begin{tabular}{| c | c | c | c | c | c |}
    \hline          
     Tagset & Parametri & Accuracy  & Tempo & MNRT& MMC  \\ \hline \hline    
    C4 & \makecell[l]{Norm: None\\ Smoot: Unknown} & $93,28\%$ & 0,206 s  &  \makecell{ \textcc{NOUN} \\ $23,47\%$ } & \makecell{ \textcc{NOUN}-\textcc{ADJ} \\ $10,04\%$ }\\ \hline     
    C4 & \makecell[l]{Norm: None\\ Smoot: Morphit} & $95,60\%$ & 0,156 s  &  \makecell{ \textcc{NOUN} \\ $24,45\%$ } & \makecell{ \textcc{NOUN}-\textcc{ADJ} \\ $15,31\%$ }\\ \hline
    C4 & \makecell[l]{Norm: Capitalize\\ Smoot: Unknown} & $93,50\%$ & 0,097 s  &  \makecell{ \textcc{NOUN} \\ $24,42\%$ } & \makecell{ \textcc{NOUN}-\textcc{ADJ} \\ $10,71\%$ }\\ \hline
    C4 & \makecell[l]{Norm: Capitalize\\ Smoot: Morphit} & \underline{$95,93\%$} & 0,136 s  &  \makecell{ \textcc{NOUN} \\ $25,14\%$ } & \makecell{ \textcc{NOUN}-\textcc{ADJ} \\ $16,05\%$ }\\ \hline
    C4 & \makecell[l]{Norm: Lemming\\ Smoot: Unknown} & $93,42\%$ & 0,119 s  &  \makecell{ \textcc{NOUN} \\ $26,78\%$ } & \makecell{ \textcc{NOUN}-\textcc{ADJ} \\ $11,74\%$ }\\ \hline
    C4 & \makecell[l]{Norm: Lemming\\ Smoot: Morphit} & $94,52\%$ & 0,148 s  &  \makecell{ \textcc{NOUN} \\ $24,01\%$ } & \makecell{ \textcc{VERB}-\textcc{NOUN} \\ $15,48\%$ }\\ \hline\hline
    C5 & \makecell[l]{Norm: None\\ Smoot: Unknown} & $92,32\%$ & 0,121 s  &  \makecell{ \textcc{VERB} \\ $18,56\%$ } & \makecell{ \textcc{PNOUN}-\textcc{NOUN} \\ $8,36\%$ }\\ \hline
    C5 & \makecell[l]{Norm: None\\ Smoot: Morphit} & \underline{$94,55\%$} & 0,193 s  &  \makecell{ \textcc{NOUN} \\ $20,36\%$ } & \makecell{ \textcc{NOUN}-\textcc{ADJ} \\ $12,58\%$ }\\ \hline
    C5 & \makecell[l]{Norm: Capitalize\\ Smoot: Unknown} & $92,46\%$ & 0,125 s  &  \makecell{ \textcc{NOUN} \\ $18,04\%$ } & \makecell{ \textcc{PNOUN}-\textcc{NOUN} \\ $23,25\%$ }\\ \hline
    C5 & \makecell[l]{Norm: Capitalize\\ Smoot: Morphit} & $94,52\%$ & 0,174 s  &  \makecell{ \textcc{NOUN} \\ $21,43\%$ } & \makecell{ \textcc{PNOUN}-\textcc{NOUN} \\ $14,69\%$ }\\ \hline
    C5 & \makecell[l]{Norm: Lemming\\ Smoot: Unknown} & $92,32\%$ & 0,128 s  &  \makecell{ \textcc{NOUN} \\ $20,12\%$ } & \makecell{ \textcc{NOUN}-\textcc{ADJ} \\ $8,36\%$ }\\ \hline
    C5 & \makecell[l]{Norm: Lemming\\ Smoot: Morphit} & $93,44\%$ & 0,162 s  &  \makecell{ \textcc{NOUN} \\ $21,07\%$ } & \makecell{ \textcc{VERB}-\textcc{NOUN} \\ $11,95\%$ }\\ \hline

  \end{tabular}
    \captionof{table}{Risultati della sperimentazione usando il tagger Viterbi.}     
    \label{tab:pos_results_2}      
\end{center}
\begin{center}
  \begin{tabular}{| c | c || c | c |}
    \hline          
     Tag & Percentuale & Coppia & Percentuale\\ \hline \hline 
\makecell[l]{\textcc{NOUN} \\ \textcc{ADJ} \\ \textcc{CONJ}  \\ \textcc{VERB} \\ \textcc{PRON} \\ \textcc{ADV} \\ \textcc{ADP}  \\ \textcc{DET} \\ \textcc{.} \\ \textcc{NUM} \\ \textcc{X}}
& \makecell{25,14\% \\ 19,79\% \\ 11,50\% \\ 10,70\% \\ 8,83\% \\ 7,76\% \\ 6,96\% \\	 4,55\% \\	 2,41\% \\ 2,14\% \\ 0,27\%} &
\makecell[l]{ \textcc{NOUN} - \textcc{ADJ}\\ \textcc{ADJ} - \textcc{NOUN}	 \\ \textcc{VERB} - \textcc{NOUN}	 \\ \textcc{CONJ} - \textcc{PRON}	\\ \textcc{ADJ} - \textcc{VERB} \\ \textcc{NOUN} - \textcc{VERB}	  \\ \textcc{PRON} - \textcc{CONJ}	  \\ \textcc{ADP} - \textcc{ADJ}	  \\ \textcc{DET} - \textcc{PRON} \\ \textcc{VERB} - \textcc{ADJ}	\\ Altre 39 coppie } & \makecell{ 16,05\%\\ 9,36\% \\ 6,15\% \\ 	 5,89\% \\ 	 5,89\% \\ 	 5,62\% \\ 	 5,09\% \\  4,02\% \\  2,68\%\\ 2,68\%\\	 36,57\%}\\ \hline
  \end{tabular}
    \captionof{table}{Le liste MNRT e MMC dei risultati più promettenti.}     
    \label{tab:pos_results_3}      
\end{center}
Nella Tabella~\ref{tab:pos_results_3} vediamo l'intera lista dei MNRT e delle MMC  della combinazione più promettente (Viterbi con tagset C4, normalizzazione \emph{Capitalize} e smoothing \emph{Morphit}).


\subsection{Valutazione dei risultati}
Guardando ai nostri dati possiamo effettuare diverse riflessioni.
\paragraph{Viterbi vs Baseline.} A parità di condizioni l'algoritmo di Viterbi batte sempre l'algortimo di Baseline. Si possono osservare importanti differenze nei tempi di esecuzione, che rimane comunque moderato. Osserviamo che l'algoritmo di Baseline tende ad assegnare il tag \textcc{NOUN} al posto di quello \textcc{VERB}: questo fatto non ci stupisce poiché i verbi ed i nomi sono le due categorie più polisemiche della lingua naturale. Inoltre, lo smoothing adottato fornisce sempre il tag \textcc{NOUN}, motivo per cui molto spesso il \textcc{VERB} è il MNRT (a percentuale molto alta).
\paragraph{L'importanza del tagset.} L'utilizzo della quinta colonna, che aggiunge i tag \textcc{PNOUN} e \textcc{AUX}, fa peggiorare le performances del tagger. Questo è giustificabile dal fatto che i tag \textcc{PNOUN} e \textcc{NOUN} si comportano sintatticamente (nei termini di probabilità di transizione) in maniera simile e quindi il tagger tende a confonderli. Il fatto è tendenzialmente confermato anche dai report relativi a MNRT e MMC (si confondono spesso \textcc{NOUN} e \textcc{PNOUN}-\textcc{NOUN}).
\paragraph{In merito alla normalizzazione.} La normalizzazione ha fornito risultati contrastanti rispetto a quanto atteso. Infatti, normalizzare mediante \emph{capitalize} ha migliorato le performances mentre invece la lemmatizzazione (che teoricamente dovrebbe aiutare di molto) ha peggiorato la accuracy. Questo fatto può essere giustificato dall'uso un po' improprio che abbiamo fatto di Morph-it!. Il lemmer mantiene al suo interno per la stessa parola tutte le forme possibili e ciò risulta in una normalizzazione di fatto randomica a fronte di ambiguità. Questo problema potrebbe essere risolto adottando strumenti più sofisticati e Word Sense Disambiguation, così da poter individuare il \emph{senso} della parola in questione e dunque la sua corretta forma normale. \\[2ex]
Infine, si osservi che la normalizzazione \emph{capitalize}, sebbene apporti una miglioria alle performances, fa sì che usando il tagset C5 e il \emph{Morphit} smoothing i nomi propri possano essere erroneamente considerati \textcc{NOUN} (ad esempio, la parola \texttt{corriere}).
\paragraph{In merito allo smoothing.} Lo smoothing è il parametro che abbiamo osservato essere in grado di influire maggiormente sulle performances. D'altronde, l'algoritmo di Viterbi si basa totalmente sulle probabilità calcolate dal corpus, quindi, le parole sconosciute costituiscono un fattore critico. Il \emph{Morphit} smoothing risulta essere il vero asso nella manica del nostro tagger.
\paragraph{Sugli errori più comuni.} Dai dati è possibile osservare come la coppia di tag maggiormente confusa sia \textcc{NOUN} e \textcc{ADJ}, il che è comprensibile poiché sono entrambi parti del discorso open class. Il tag più difficilmente riconosciuto è \textcc{NOUN}: ciò è motivato dal fatto che tendenzialmente le parole nuove (alle quali è quindi più difficile assegnare un tag) sono nomi.






