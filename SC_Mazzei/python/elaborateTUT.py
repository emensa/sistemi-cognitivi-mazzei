# This Python file uses the following encoding: utf-8
__author__ = 'Davide, Enrico'

import sys
from nltk.tree import Tree, ParentedTree
from nltk import induce_pcfg
from nltk import Nonterminal
import re
import math
import io


#Verranno forniti dalla chiamata Java
tut_dirty_path = "";
tut_clean_train_path = "";
tut_clean_test_path = "";
grammar_path = "";
test_set_CNFtree_path = "";
test_set_flatten_path = "";

#usati solo in pos tagging mode
ENABLE_POS_TAGGING = False; #cambia diverse parti del flow
train_set_CNFcolumns_4PoS_path = "";

#Percentuale di corpus dedicato al training (il restante per il test)
train_percent = 0.9;


#Il main: lancia tutte le elaborazioni.
def elaborateTUT():
    #import sys
    #reload(sys) 
    #sys.setdefaultencoding('utf-8')
    tut_dirty_path  = sys.argv[1]; #il corpus (sporco)
    tut_clean_train_path = sys.argv[2]; #il 90% del corpus (pulito), usato per il training
    tut_clean_test_path = sys.argv[3]; #il 10% del corpus (pulito), usato per il testing (gold standard)
    grammar_path = sys.argv[4]; #file che contiene la grammatica estratta dal training set
    test_set_CNFtree_path = sys.argv[5]; #file che contiene una serie di alberi (di test)
    test_set_flatten_path= sys.argv[6]; #file che contiene tutte le foglie (frasi), una per riga

    global ENABLE_POS_TAGGING
    
    #verifica sul parametro di pos tagging esterno
    if len(sys.argv) == 9:
        if sys.argv[7] == '-extpos':
            train_set_CNFcolumns_4PoS_path = sys.argv[8]
            ENABLE_POS_TAGGING = True;


    #print train_set_CNFcolumns_4PoS_path

    ## ------ PASSO 0: PULISCO IL CORPUS ----------
    #ottengo gli alberi puliti
    trees = readAndCleanTrees(tut_dirty_path, True) #alberi puliti del trainingset


    ## ------ PASSO 0.1: RENAME DEI POS SE IN MODALITÀ POS TAGGING ----------                           
    # sostituisco tutti i tag
    if ENABLE_POS_TAGGING:  
        for tree in trees:
            renameLabelsToGooglePoS(tree)


    ## ------ PASSO 1: CREAZIONE TRAIN E TEST SET ----------
    # Creo il training set ed il test set
    # calcolo il 90 percento della length
    # creo i due file e scrivo
    
    total_trees = len(trees);
    n_train_trees = math.ceil(total_trees*train_percent);
    n_test_trees = total_trees - n_train_trees;

    # alberi train e set (stringhe)
    str_train_trees = ""
    str_test_trees = ""

    # alberi train e set (oggetti)
    train_trees = []
    test_trees = []

    i = 0;
    for tree in trees:
        if i <= n_train_trees:
            str_train_trees += tree_to_string(tree).encode("utf-8")+"\n"
            train_trees.append(tree)
        else:
            str_test_trees += tree_to_string(tree).encode("utf-8")+"\n"  
            test_trees.append(tree)
        
        i += 1;
        
    #scrivo su file tutti i miei trees e ho train set e test set
    open(tut_clean_train_path, 'w').write(str_train_trees)
    open(tut_clean_test_path, 'w').write(str_test_trees)

    ## ------ PASSO 2: GENERO LA GRAMMATICA ----------
    produzioni = []
    for tree in train_trees:
        tree = Tree.fromstring(tree_to_string(tree)) #converto il ParentedTree in Tree
        #Tree.chomsky_normal_form(tree)
        #Tree.collapse_unary(tree)
        tree.chomsky_normal_form()
        tree.collapse_unary(collapsePOS=True)
        new_produzioni =  tree.productions()
        
        if ENABLE_POS_TAGGING: #se devo fare pos rimuovo le produzioni lessicalizzate
            for new_produzione in new_produzioni:
                if not new_produzione.is_lexical():
                    produzioni.append(new_produzione)
        else: #altrimenti van tutte bene
            produzioni += new_produzioni

    # la grammatica
    grammar = induce_pcfg(Nonterminal('H'), produzioni)
    # print grammar
    strgrm = str(grammar)

    open(grammar_path, 'w').write(manually_encode_UTF8_string(strgrm)) #encoding manuale, quello di python non funziona

    
    ## ------ PASSO 3: GENERO I FILE RELATIVI AL TEST ----------

    #Il train set
    chmosky_test_trees = ""
    flatten_test_trees = ""    
    for tree in test_trees:
        tree = Tree.fromstring(tree_to_string(tree)) #converto il ParentedTree in Tree
        tree.collapse_unary(collapsePOS=True)
        tree.chomsky_normal_form()

        chmosky_test_trees += tree_to_string(tree)+"\n" #Alberi interi del train set
        flatten_test_trees += tree_to_sentence(tree)+"\n" # Solo le frasi

    open(test_set_CNFtree_path, 'w').write(chmosky_test_trees.encode("utf-8"))
    open(test_set_flatten_path, 'w').write(flatten_test_trees.encode("utf-8"))


    ## ------ PASSO 4: SOLO PER POS TAGGING, TRAINING FILE ---------
    if ENABLE_POS_TAGGING:
        pos_string = ""    
        for tree in train_trees:
            tree = Tree.fromstring(tree_to_string(tree)) #converto il ParentedTree in Tree
            tree.chomsky_normal_form()
            tree.collapse_unary(collapsePOS=True)
            
            new_produzioni =  tree.productions()

            i = 1;
            for produzione in new_produzioni:
                if produzione.is_lexical():
                    split_prod = str(produzione).split(" ")
                    word = split_prod[2][1:-1]
                    tag = split_prod[0]                    
                    pos_string += str(i)+"\t"+word+"\t_\t"+tag+"\n"
                    i += 1

            pos_string += "\n" #linea vuota, fine frase


        open(train_set_CNFcolumns_4PoS_path, 'w').write(manually_encode_UTF8_string(pos_string))
    

#Legge il treebank sporco e richiama la pulizia
def readAndCleanTrees(treebank_path, parented=False):
    treebank = open(treebank_path).read().strip().split("\n")
    trees = []
    for line in treebank:
        line = line.decode("utf-8")

        if parented:
            trees.append(ParentedTree.fromstring(line))
        else:
            trees.append(Tree.fromstring(line))

    return clean(trees)


#Pulisce una serie di alberi
def clean(trees):
 
    def navigate_tree(root, function):
        for node in root:
            if isinstance(node, Tree):
                navigate_tree(node, function)
                function(node)
 
    def clean_label(x):
        m = re.search('(.*)(?=-[0-9]+)(?=.*)', x.label())
        if m:
            x.set_label(m.group(0))
 
    def delete(x):
        if x.label() in {'-NONE-', '-LRB-', '-RRB-'}:
            toprune.append(x)
 
    def delete_repeatitions(x):
        if x.label() in {'PREP', 'VMA', ','}:
            bro = x.right_sibling()
            if bro:
                while isinstance(bro[0], Tree):
                    bro = bro[0]
                if x[0] == bro[0]:
                    toprune.append(bro)
                    uncle = bro.parent().right_sibling()
                    if x.label() == 'VMA' and uncle:
                        if bro[0] == uncle[0][0]:
                            toprune.append(uncle[0])
 
    def delete_node(toprune):
        for node in toprune:
            while len(node.parent()) == 1:
                node = node.parent()
            node.parent().remove(node)
 
    for tree in trees:
        # print(' '.join(tree.leaves()))
        tree.set_label('H')
        toprune = []
        navigate_tree(tree, clean_label)
        navigate_tree(tree, delete)
        navigate_tree(tree, delete_repeatitions)
        delete_node(toprune)
        # print(' '.join(tree.leaves()))
 
    return trees

#Trasforma un albero in stringa
def tree_to_string(tree):
    if tree:
        return Tree.pformat(tree, margin=sys.maxsize)
    else:
        return ''

def tree_to_sentence(tree):
    if tree:
        return ' '.join(tree.leaves())
 
 
def trees_to_sentence(trees):
    sentences = []
    for tree in trees:
        sentence = tree_to_sentence(tree)
        sentences.append(sentence.lower())
    return sentences

def manually_encode_UTF8_string(string):
    string = string.replace("\\xe9", "é").replace("\\xe0", "à").replace("\\xec", "ì").replace("\\xf9", "ù").replace("\\xe8", "è").replace("\\xf2","ò")
    return string.replace("\\xc0", "À").replace("\\xd9", "Ù").replace("\\xc8","È").replace("\\xb0", "°")
    
# Converte un tag del TUT in GooglePoS
def toGooglePoS(text):
    replacements = {
        # ARTICOLI
        'ART': 'DET',
        # AVVERBI
        'ADVB': 'ADV',
        # VERBI
        'VAU': 'VERB', 'VCA': 'VERB', 'VMO': 'VERB', 'VMA': 'VERB',
        # NOMI
        'NOU': 'NOUN',
        # PREPOSIZIONI
        'PREP': 'ADP',
        # PRONOMI
        'PRO': 'PRON',
        # NUMERI
        'NUMR': 'NUM',
        # PUNCT
        'PUNCT': '.'
    }

    for repl_old, rep_new in replacements.items():
        text = text.replace(repl_old, rep_new)
    return text


def renameLabelsToGooglePoS(root):
    for node in root:
        if isinstance(node, Tree):
            renameLabelsToGooglePoS(node)
            node.set_label(toGooglePoS(node.label()))     

if __name__ == "__main__":
    elaborateTUT()

