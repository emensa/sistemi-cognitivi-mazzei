
### Viterbi - Tagset: C5, Normalization: CAPITALIZE, Smoothing: MORPHIT ### 


Seen words:	9187
Correct tags:	8683
Tag accuracy:	94,52%
Elapsed time:	0.174 secs


Statistic on not recognized tags:

NOUN	--> 21,43%
PNOUN	--> 18,06%
ADJ	--> 13,89%
VERB	--> 12,31%
CONJ	--> 8,14%
PRON	--> 6,35%
ADV	--> 5,16%
ADP	--> 4,97%
DET	--> 3,18%
AUX	--> 2,78%
.	--> 1,99%
NUM	--> 1,59%
X	--> 0,20%


Statistic on mismatched tag couples (was in corpus - was tagged):

PNOUN - NOUN	--> 14,69%
NOUN - ADJ	--> 12,11%
NOUN - PNOUN	--> 4,97%
VERB - AUX	--> 4,77%
CONJ - PRON	--> 4,37%
VERB - NOUN	--> 3,97%
PRON - CONJ	--> 3,77%
ADJ - VERB	--> 3,58%
ADJ - NOUN	--> 3,38%
ADJ - PNOUN	--> 3,38%
ADP - ADJ	--> 2,78%
VERB - ADJ	--> 2,78%
NOUN - VERB	--> 2,58%
AUX - VERB	--> 2,19%
. - ADJ		--> 1,99%
ADJ - DET	--> 1,99%
DET - PRON	--> 1,79%
ADV - CONJ	--> 1,59%
CONJ - ADV	--> 1,39%
PRON - DET	--> 1,39%
CONJ - ADJ	--> 1,39%
ADV - NOUN	--> 1,20%
NUM - NOUN	--> 1,20%
ADP - ADV	--> 1,00%
PRON - ADJ	--> 1,00%
PNOUN - DET	--> 1,00%
DET - ADJ	--> 0,80%
PNOUN - ADJ	--> 0,80%
PNOUN - ADP	--> 0,80%
ADP - CONJ	--> 0,60%
ADJ - ADV	--> 0,60%
NOUN - NUM	--> 0,60%
ADV - ADJ	--> 0,60%
PNOUN - VERB	--> 0,40%
CONJ - ADP	--> 0,40%
ADV - NUM	--> 0,40%
NUM - PNOUN	--> 0,40%
ADJ - ADP	--> 0,40%
DET - ADV	--> 0,40%
ADJ - PRON	--> 0,40%
ADV - PRON	--> 0,40%
CONJ - VERB	--> 0,40%
ADV - ADP	--> 0,40%
VERB - ADP	--> 0,40%
AUX - ADJ	--> 0,40%
NOUN - DET	--> 0,40%
PRON - ADV	--> 0,20%
VERB - PRON	--> 0,20%
ADP - NOUN	--> 0,20%
NOUN - ADV	--> 0,20%
NOUN - ADP	--> 0,20%
ADP - PNOUN	--> 0,20%
ADV - DET	--> 0,20%
DET - ADP	--> 0,20%
X - PNOUN	--> 0,20%
NOUN - AUX	--> 0,20%
ADV - VERB	--> 0,20%
PNOUN - PRON	--> 0,20%
AUX - ADV	--> 0,20%
CONJ - NOUN	--> 0,20%
VERB - ADV	--> 0,20%
ADP - DET	--> 0,20%
ADJ - NUM	--> 0,20%
NOUN - .	--> 0,20%
PNOUN - AUX	--> 0,20%
ADV - PNOUN	--> 0,20%



