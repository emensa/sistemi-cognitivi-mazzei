
### Viterbi - Tagset: C4, Normalization: CAPITALIZE, Smoothing: UNKNOWN ### 


Seen words:	9187
Correct tags:	8589
Tag accuracy:	93,50%
Elapsed time:	0.097 secs


Statistic on not recognized tags:

NOUN	--> 24,42%
ADJ	--> 22,25%
VERB	--> 18,23%
CONJ	--> 7,53%
PRON	--> 7,20%
ADV	--> 5,86%
NUM	--> 5,19%
ADP	--> 4,69%
DET	--> 2,85%
.	--> 1,68%
X	--> 0,17%


Statistic on mismatched tag couples (was in corpus - was tagged):

NOUN - ADJ	--> 10,71%
VERB - NOUN	--> 8,37%
ADJ - NOUN	--> 8,20%
ADJ - .		--> 5,19%
NOUN - ADP	--> 4,19%
CONJ - PRON	--> 3,85%
ADJ - VERB	--> 3,85%
NOUN - VERB	--> 3,85%
NUM - NOUN	--> 3,68%
PRON - CONJ	--> 3,68%
VERB - .	--> 3,35%
NOUN - DET	--> 3,18%
VERB - ADP	--> 3,02%
ADP - ADJ	--> 2,51%
ADJ - ADP	--> 2,35%
PRON - DET	--> 2,18%
VERB - ADJ	--> 2,18%
ADJ - DET	--> 1,84%
. - ADJ		--> 1,68%
NOUN - .	--> 1,68%
DET - PRON	--> 1,51%
ADV - NOUN	--> 1,51%
ADV - CONJ	--> 1,18%
CONJ - ADV	--> 1,18%
CONJ - ADJ	--> 1,18%
VERB - DET	--> 1,01%
ADP - ADV	--> 1,01%
PRON - ADJ	--> 1,01%
DET - ADJ	--> 0,84%
ADV - ADP	--> 0,84%
ADV - ADJ	--> 0,84%
ADP - CONJ	--> 0,67%
ADV - VERB	--> 0,67%
CONJ - ADP	--> 0,51%
NUM - DET	--> 0,51%
ADJ - PRON	--> 0,51%
CONJ - VERB	--> 0,51%
NUM - ADP	--> 0,51%
ADP - NOUN	--> 0,34%
ADV - NUM	--> 0,34%
NOUN - ADV	--> 0,34%
ADJ - ADV	--> 0,34%
DET - ADV	--> 0,34%
CONJ - NOUN	--> 0,34%
ADV - PRON	--> 0,34%
NUM - ADJ	--> 0,34%
NOUN - NUM	--> 0,34%
VERB - ADV	--> 0,34%
PRON - NOUN	--> 0,34%
X - NOUN	--> 0,17%
NOUN - PRON	--> 0,17%
DET - ADP	--> 0,17%
ADV - .		--> 0,17%
NUM - .		--> 0,17%
ADP - DET	--> 0,17%



