/*
 * Classe che permette di testare lo YodishSpeaker.
 */
package transferMT;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Enrico
 */
	
import java.util.Scanner;

public class TestYoda {
	
	private static final boolean NO_INTERFACE = false; //disattiva l'interfaccia dialogante 

	public static void main(String[] args) throws Exception {
		YodishSpeaker yoda; 
		
		if(NO_INTERFACE) {
			System.out.print("Attendi: carico librerie\n");
			yoda = YodishSpeaker.getInstance();
			yoda.test();
			System.out.println("\n\n## Termino con System Exit. ##");
			System.exit(1);
		} 
	
		LibraryLoading load = new LibraryLoading();
					
		load.start(); yoda = YodishSpeaker.getInstance(); load.stop();
		
		String[] built_in_sentences = new String[]{ "The mind of a child is truly wonderful.", 
													"You still have much to learn.", 
													"The council does agree with you.", 	
													"Master Obi-Wan has lost a planet.",
													"The Clone Wars has begun.",
													"There is no time to question.",
													"Do not mourn them. Do not miss them."};

		try (Scanner scanner = new Scanner(System.in)) {
			int menu_option = 0;
			System.out.println("\n\n\033[1m* Ready!"); think(1);
			System.out.println("\n\033[1m* Hi. A Yoddish native speaker I am."); 							
				
			do {
				think(1);				
				System.out.println("\n\033[1m* How serve you can I? 1 press to sentence insert, 2 to random sentence get.\n\033[0m");
				menu_option = scanner.nextInt();
				String text = "";
				
				if (menu_option == 1) {
					scanner.nextLine();					
					System.out.println("\n\033[1m* Ok, young padawan. Your sentence here insert:\n\033[0m\033[32;1;2m");					
					text = scanner.nextLine();		
					
					//Traduco
					String yoddish_text = yoda.translateFromEnglish(text);
					
					System.out.print("\n\033[0m\033[1m* Thinking I am");
				
					threeSuspensionDots(0.8); //pensa coi puntini				
						
					System.out.println("\n\n\033[1m* Your translation here it is:\n\033[0m");
					System.out.println("\033[32;1;2m"+yoddish_text);					
				
				} else if (menu_option == 2) { //random
					int sentence_index = ThreadLocalRandom.current().nextInt(0, built_in_sentences.length-1);
					text = built_in_sentences[sentence_index];
					System.out.println("\n\033[1m* For you the sentence '\033[32;1;2m"+text+"\033[0m\033[1m' I have chosen.\n");										
					think(1);
					String yoddish_text = yoda.translateFromEnglish(text);					
					System.out.print("\n\033[1m* '\033[32;1;2m"+yoddish_text+"\033[0m\033[1m'.");							
					System.out.println(" \033[1mThat's how it I would say.");
					
					
				} else if (menu_option == 0) {
					
					System.out.println("\n\033[34m# Ok, let's make it fast ;) Insert your phrase:\033[32;1;2m\n");
					scanner.nextLine();					
					text = scanner.nextLine();	
					
					String yoddish_text = yoda.translateFromEnglish(text);
					System.out.println("\n\033[34m# Translation: \033[32;1;2m"+yoddish_text);					
					
				}

				System.out.println("\n\n\033[0m\033[1m* Again? 1 to start over, 2 to shut me up.\033[0m\n");
				menu_option = scanner.nextInt();
			} while (menu_option != 2);
			System.out.print("\n\033[1m* Okay. Bye Bye.\n\n\033[0m");
		}
	}
	

	/**
	 * Thread che stampa mentre vengono create le librerie
	 */
	protected static class LibraryLoading extends Thread { 
		public void run() {
			System.out.print("\n\033[1m* Oh"); threeSuspensionDots(0.8); System.out.print("\n\n\033[1m* "); 
			threeSuspensionDots(0.8);
			System.out.print("\033[1ma second to warm up you give me"); think(0.8);
			while(true) {
				System.out.print("\033[1m.");
				think(1);
			}
		}
	}
	
	/**
	 * Simula il pensiero per n secondi
	 * @param secs 
	 */
	public static void think(double secs){
		try {
			Thread.sleep((int) Math.floor(secs*1000));             
		} catch(InterruptedException ex) {
			Thread.currentThread().interrupt();
		}	
	}
	
	/**
	 * Scrive tre puntini di sospensione.
	 * 
	 * @param secs 
	 */
	public static void threeSuspensionDots(double secs){
		int i = 1; while(i <= 3) { i++; think(secs); System.out.print("\033[1m."); } think(1);
	}
	
	/**
	 * Simula la scrittura della stringa s come un umano.
	 * @param s 
	 */
	public static void humanWriting(String s) {
		for (char ch: s.toCharArray()) {
			System.out.print(ch);
			think(0.3);
		}
	}
	

}

