/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transferMT;

import NLPutil.MiscUtil;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.LabeledScoredTreeNode;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author Enrico
 */
public class YodishSpeaker {
	
	private static YodishSpeaker instance = null;
	
	private static StanfordCoreNLP pipeline;
		
	private static final boolean DEBUG_MODE = false;

    
	public YodishSpeaker() {}

	
    public static YodishSpeaker getInstance() {
        if (instance == null) {
            instance = new YodishSpeaker();
			// creates a StanfordCoreNLP object, with POS tagging, lemmatization, parsing 
			Properties props = new Properties();
			props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse");

			if(!DEBUG_MODE) System.err.close(); //        
			pipeline = new StanfordCoreNLP(props);			
        }
        return instance;			
    }
	
	/**
	 * Traduce una frase dall'inglese allo Yodish e la restituisce. 
	 * 
	 * @param sentence
	 * @return 
	 */
    public String translateFromEnglish(String sentence) {
				
        // create an empty Annotation just with the given text
        Annotation document = new Annotation(sentence);
		        // run all Annotators on this text
		pipeline.annotate(document);
        
        String result ="";
		String original_sexpr = "";
		String yodish_sexpr = "";		
		
        // these are all the sentences in this document (1 sola)
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);

		for (CoreMap word : sentences) {
			// costruisce l'albero
			Tree tree = word.get(TreeCoreAnnotations.TreeAnnotation.class);

			original_sexpr += tree.toString() + "\n";
			translateTree(tree);
			yodish_sexpr += tree.toString() + "\n";

			result += rebuildSentenceFromWords(tree.yieldWords()) + " ";
		}
        
		
		if(DEBUG_MODE) {
			System.out.println("Input phrase: "); MiscUtil.pprintSExpression(original_sexpr);
			System.out.println("Translated version: "); MiscUtil.pprintSExpression(yodish_sexpr);
			System.out.println("Input phrase: "+sentence);
			System.out.println("Translated version: "+result);
		}
		
		return result;

    }
	
	/**
	 * Data una frase ne restituisce la SExpression.
	 * 
	 * @param sentence
	 * @return 
	 */
	public String getSExpression(String sentence) {
				
        // create an empty Annotation just with the given text
        Annotation document = new Annotation(sentence);
		// run all Annotators on this text
		pipeline.annotate(document);

		String result ="";
		
        // these are all the sentences in this document (1 sola)
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
		
		for (CoreMap word : sentences) {
            
			// costruisce l'albero
			Tree tree = word.get(TreeCoreAnnotations.TreeAnnotation.class);
			
			result += tree.toString()+"\n";
		}

		return result;

    }
	
	
	
	/**
	 * Visita l'albero e lo traduce in Yodish mediante l'uso di alcune semplici regole.
	 * 
	 * Implementiamo due regole (si tralascino ROOT ed S nelle indicazioni):
	 * A) Se la frase è del tipo '(VP_1 ( * VP_2))' diventa del tipo '(VP_2) (VP_1 *)'
	 * B) Se la frase è del tipo '*_1 NP *_2 (VP (T1 T2 *_3)) *_4' diventa del tipo '*_1 T2 *_3 NP *_2 (VP (T1)) *_4'
	 * 
	 * Con * indichiamo qualsiasi altra sequenza
	 * 
	 * @param t
	 */
	private static Tree translateTree(Tree tree) {
		//caso base
		if (tree.isEmpty() || tree.isLeaf()) 
			
			return tree;
		
		else { // è da processare

			Tree[] childs = tree.children(); //figli di tree
			
			if (childs.length == 2 && equalsPoS(childs[0], "VP")) { // potremmo riuscire ad applicare la regola A
				
				Tree VP_1 = childs[0];
				Tree[] VP_1_childs = VP_1.children(); //figli di tree			
				
				//scorro i figli e cerco il primo VP
				for (int i = 0; i < VP_1_childs.length; i++) {
					if (equalsPoS(VP_1_childs[i], "VP")){ // ok, possiamo applicare A
						Tree VP_2 = VP_1_childs[i];

						tree.addChild(0, VP_2);
						tree.addChild(1, getCommaTree()); //aggiungo la virgola													
						VP_1.removeChild(i);
					}
				} // </for>
			
				
			} else { //vediamo un se riusciamo ad applicare altro 
				
				int np_index = -1;
				
				//Scorro i figli e cerco il primo np
				for (int i = 0; i < childs.length; i++) {
					if (equalsPoS(childs[i], "NP")){ // potremmo riuscire ad applicare la regola B
						np_index = i; //mi segno che np si trova in posizione i
					} 
					
					if(equalsPoS(childs[i], "VP") && np_index != -1 && i > np_index) { //ho trovato un VP successivo a NP, vediamo se riusciamo ad applicare B
						Tree NP = childs[np_index];
						Tree VP = childs[i];

						Tree[] VP_childs = VP.children(); //figli di tree

						if (VP_childs.length > 1) { //se ci sono almeno due figli: posso applicare B
							
							tree.addChild(np_index, getCommaTree()); //aggiungo la virgola														
							
//							Tree base_verb = null; //potrebbe essere utile per serbare la base del verbo
							
							for(int k = VP_childs.length-1; k >= 1; k--) {
								
								//VERBI COMPOSTI: 
								//Andiamo a verificare se il k-esimo figlio di VP che stiamo trattando è un VP ed ha il primo figlio che è un VBN oppure un VB
//								if (equalsPoS(VP_childs[k], "VP")) { //il k-esimo figlio è un VP
//									Tree innerVP = VP_childs[k];
//									if(equalsPoS(innerVP.getChild(0), "VB") || equalsPoS(innerVP.getChild(0), "VBN")) { //è la base del verbo, non lo vogliamo muovere!
//										base_verb = innerVP.getChild(0); //lo salvo
//										innerVP.removeChild(0); //lo rimuovo dall'innerVP
//									}
//								}
							
								
								Tree Tk = VP_childs[k];							

								 tree.addChild(np_index, Tk); //metto il secondo figlio di VP (T2) davanti a NP (se non era base verb)
								VP.removeChild(k); //rimuovo il secondo figlio di VP (T2) da VP	
								
//								if(base_verb != null) break; //ho trovato un verbo base, interrompo la cancellazione dei figli 
							} 
							
							//Se ho trovato una forma base, hHo mosso tutti i figli di VP dal fondo fino alla forma base stessa: la reintegro (il resto in mezzo è ancora lì)
//							if(base_verb != null) VP.addChild(base_verb);
							
						}
					}
				} // </for>
			}			
		
			//Chiamata ricorsiva
			for (Tree child : tree.children())
					translateTree(child);
		}
			
		return tree; //ritorno l'albero modificato
		
		
	}
	
	
	/**
	 * Data una lista di oggetti Word ricostruisce la frase.
	 * 
	 * @param words
	 * @return
	 */
	private static String rebuildSentenceFromWords(List<Word> words) {
		String result = "";
		int i = 0;
		for (Word word : words) {
			if (i == 0)
				result += " " + StringUtils.capitalize(word.word());
			else if (word.word().matches("[,.:;!?]"))
				result += word.word();
			else
				result += " "+word.word();
			i++;
		}

		return result;
	}	
	
	
	/**
	 * Dice se la root di un certo albero è un determinato tag passato come parametro.
	 * 
	 * @param t
	 * @param pos
	 * @return
	 */
	private static boolean equalsPoS(Tree t, String pos) {
		if (t.isLeaf())
			return false;

		return t.label().value().matches(pos);
	}
	
	/**
	 * Restituisce un albero con un nodo solo costituito da una virgola.
	 * @return 
	 */
	private static LabeledScoredTreeNode getCommaTree () {
		CoreLabel comma = new CoreLabel();
		comma.setValue(",");
		CoreLabel l = new CoreLabel();
		l.setValue(".");
		List<Tree> childComma = new ArrayList<>();
		childComma.add(new LabeledScoredTreeNode(comma));
		
		return new LabeledScoredTreeNode(l, childComma);
	}
    
    /**
	 * Metodo di utilità che testa la classe senza usare alcuna interfaccia.
	 */
	public void test() {
		
//		translateFromEnglish("There are always two, no more.");
//		System.out.println("----------------------------------------");	
//		
//		translateFromEnglish("The mind of a child is truly wonderful."); 
//		System.out.println("----------------------------------------");		
//		
//		translateFromEnglish("You still have much to learn."); 
//		System.out.println("----------------------------------------");	
//		
		translateFromEnglish("The council does agree with you."); 
		System.out.println("----------------------------------------");	
//		
		translateFromEnglish("Skywalker will be your apprentice."); 
		System.out.println("----------------------------------------");		
//
//		translateFromEnglish("Master Obi-Wan has lost a planet.");
//		System.out.println("----------------------------------------");	
//		
//		translateFromEnglish("The Clone Wars has begun.");
//		System.out.println("----------------------------------------");
//		
//		translateFromEnglish("There is no time to question.");
//		System.out.println("----------------------------------------");		
		
		translateFromEnglish("Young Skywalker has become twisted by the Dark Side.");
		System.out.println("----------------------------------------");		

//		translateFromEnglish("The boy you trained, he is gone, consumed by Darth Vader."); 
//		System.out.println("----------------------------------------");
//
//		translateFromEnglish("Do not mourn them. Do not miss them.");
//		System.out.println("----------------------------------------");

		



		
		
//		MiscUtil.pprintSExpression(getSExpression("The Clone Wars has begun."));		
//		MiscUtil.pprintSExpression(getSExpression("Begun the Clone Wars has."));
//		System.out.println("----------------------------------------");
//		MiscUtil.pprintSExpression(getSExpression("Skywalker will be your apprentice."));		
//		MiscUtil.pprintSExpression(getSExpression("Your apprendice Skywalker will be."));		
//		System.out.println("----------------------------------------");
//		MiscUtil.pprintSExpression(getSExpression("The mind of a child is truly wonderful."));		
//		MiscUtil.pprintSExpression(getSExpression("Truly wonderful, the mind of a child is."));			
		
	}

    
}
