/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package postagger;

import NLPutil.UDTPTaggingUtil;
import static NLPutil.UDTPTaggingUtil.UDTP_TransitionEmissionProbability;
import static NLPutil.UDTPTaggingUtil.UDTP_WordTagCouples;
import NLPutil.MorphitWrapper;
import NLPutil.WordTagCouple;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Enrico
 */
public class ViterbiPoSTagger {
    

	/**
     * #################
     * #################
     * 
     * Variabili di esecuzione
     * 
     * #################
     * #################
     */	
	
    private static final String TAGGER_NAME = "Viterbi";
    
    //Possibili smoothing:
    //UNKNOWN --> P(word|tag) = 1/(numero di tag)
	//MORPHIT --> distribuzione di probabilità ottenuta da morphit
    //GOODTURING ---> TODO
    private static final String SMOOTH_TYPE = "MORPHIT";
    
    //Possibili normalizzazioni:
    //NONE --> nessuna normalizzazione
    //CAPITALIZE --> tutte le parole sono rese minuscole
	//LEMMING --> le parole sono ridotte alla forma normale	mediante Morphit
    private static final String NORMALIZATION_TYPE = "LEMMING";
    
    //Possibili tagset:
    //C4 --> tag presi dalla colonna 4
    //C5 --> tag presi dalla colonna 5 
    private static final String TAGSET_TYPE = "C4";
    
    private static final String method_name = TAGGER_NAME+ " - Tagset: " + TAGSET_TYPE + ", Normalization: "+NORMALIZATION_TYPE+", Smoothing: "+SMOOTH_TYPE;
    
    
	/**
     * #################
     * #################
     * 
     * Files
     * 
     * #################
     * #################
     */

	
    //Training set & test set
    private static final String train_set_path = "corpus/pos/it-universal-train.conll";
    private static final String test_set_path = "corpus/pos/it-universal-test.conll";    
    
    
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
		
		// ### Dichiarazioni variabili 
		
		//Lista dei tag considerati (verrà calcolata in automatico dal corpus)
		ArrayList<String> tag_list; 

		// Struttura che mappa la transition probability <t_{i-1}, t_i> ossia P(t_i | t_{i-1}).
		// Sulle righe abbiamo i tag (t_{i-1}) con l'aggiunta di S0 tag di start, mentre sulle colonne abbiamo i tag (senza S0).
		HashMap<String, HashMap<String, Double>> transition_probability;


		// Struttura che mappa la emission probability <t_i, w_i> ossia P(w_i|t_i).
		// Sulle righe abbiamo le tag, mentre sulle colonne abbiamo le parole (ovviamente senza S0). 
		HashMap<String, HashMap<String, Double>> emission_probability;			
		
        try {	
			
			//Inizializziamo morphit prima di tutto così da non perdere tempo nell'algoritmo
			if(SMOOTH_TYPE.equals("MORPHIT") || NORMALIZATION_TYPE.equals("LEMMING")) MorphitWrapper.getInstance();

			//Inizializzo la tag list
			tag_list = UDTPTaggingUtil.UDTP_ExtractTagList(train_set_path, TAGSET_TYPE);

			//System.out.println(tag_list);

			//Popolo la struttura base
			ArrayList<HashMap<String, HashMap<String, Double>>> structures = UDTP_TransitionEmissionProbability(train_set_path, tag_list, TAGSET_TYPE, NORMALIZATION_TYPE);
			transition_probability = structures.get(0);
			emission_probability = structures.get(1);

			//System.out.println("Transition probability\n\n"+transition_probability);
			//System.out.println("Emission probability\n\n"+emission_probability);        


			//Lettura del test set
			ArrayList<ArrayList<WordTagCouple>> test_set = UDTP_WordTagCouples(test_set_path, TAGSET_TYPE, NORMALIZATION_TYPE);

			//Risultato di viterbi
			ArrayList<ArrayList<WordTagCouple>> viterbi_result = new ArrayList<>();
			long viterbi_elapsed_time = 0;

			for (ArrayList<WordTagCouple> one_phrase_al : test_set) {
				//converto l'arraylist in String[] 
				String[] one_phrase = new String[one_phrase_al.size()];
				for (int i = 0; i < one_phrase_al.size(); i++) {
					one_phrase[i] = one_phrase_al.get(i).getWord();
				}
				//Applicazione dell'algoritmo di viterbi
				long startTime = System.currentTimeMillis();
				ArrayList<WordTagCouple> one_viterbi_result = viterbiPoSTagger(one_phrase, tag_list, transition_probability, emission_probability);
				viterbi_elapsed_time += (System.currentTimeMillis()-startTime);

				viterbi_result.add(one_viterbi_result); //aggiungo al risultato finale per fare evaluation

			}
			//System.out.println("Viterbi result:" + viterbi_result);

			NLPutil.EvaluateUtil.printEvaluationForMethod(method_name, viterbi_result, test_set, ""+(double) viterbi_elapsed_time/1000);
              
        } catch(Exception ex) {
            ex.printStackTrace(System.out);
        }    }
    
       
    /**
     * Viterbi PoS tagger.
     * Sono implementate diverse misure di smoothing (righe dopo: commento e scommento).
     * 
     * Il parametro test_set contiene già le risposte al tag, 
     * ma viene usato solo per inizializzare in maniera consistente il risultato.
     * 
     * Assumiamo che tutti i tag della tag list siano stati utilizzati almeno una volta nel corpus.
     * 
     * @param phrase
	 * @param tag_list
	 * @param transition_probability
	 * @param emission_probability
     * @return 
     */
    public static ArrayList<WordTagCouple> viterbiPoSTagger(String[] phrase, ArrayList<String> tag_list, HashMap<String, HashMap<String, Double>> transition_probability, HashMap<String, HashMap<String, Double>> emission_probability) {
        
        ArrayList<WordTagCouple> result = new ArrayList<>();
        
        //Formalismo sugli indici: i tag (t) vanno da 0 a T, le parole (w) vanno da 0 a N.

        int T = tag_list.size()-1; //ultimo indice dei tag
        int N = phrase.length-1; //ultimo indice delle parole
        
        //viterbi deve contenere T * N elementi (dimensionato sulle length)
        double[][] viterbi = new double[tag_list.size()][phrase.length]; //viterbi[i][k] probabilità del miglior path path per le prime k osservazioni e che finisce nello stato i dell'HMM
        int[][] backpointer = new int[tag_list.size()][phrase.length]; //backpointer[i][k] per ogni k (ogni colonna, ossia ogni parola) mantieniamo il tag che ha massimizzato fino a lei
        
        
        for(int i = 0; i<=T; i++) { //riempio la prima (la 0-esima) colonna di viterbi
            viterbi[i][0] =  transition_probability.get("S0").get(tag_list.get(i)) * smoothed_emission_probability(tag_list.get(i), phrase[0], tag_list, emission_probability); //viterbi(i,0) = P(tag_list[0]| "S0") * P(phrase[0] | tag_list[0]) 
            //System.out.println("TRA:\t"+completed_transition_probability("S0", tag_list.get(i))+ " Per S0 e "+tag_list.get(i));
            //System.out.println("EMI\t"+smoothed_emission_probability(tag_list.get(i), phrase[0])+" Per "+tag_list.get(i)+" e "+phrase[0]);
            backpointer[i][0] = -1;
        }
        for(int t = 1; t <= N; t++) { //le altre colonne (dalla 1 alla N)
            for(int i = 0; i <= T; i++) {//tutte le righe
                
                //Calcolo il massimo
                int best_index = 0; //per backpoint
                double best_prob = 0;
                for(int j = 0; j <= T; j++) { //tutti i tag
                    double actual_prob = viterbi[j][t-1] * transition_probability.get(tag_list.get(j)).get(tag_list.get(i));
                    if(actual_prob > best_prob) {
                        best_prob = actual_prob;
                        best_index = j;
                    }
                }    
                viterbi[i][t] = best_prob * smoothed_emission_probability(tag_list.get(i), phrase[t], tag_list, emission_probability); //la probabilità di emissione è fuori dal massimo poiché costante rispetto ad esso
                backpointer[i][t] = best_index;

            }
        }
        
       // NLPutil.MiscUtil.printMatrix(viterbi);
        
        
        //Ricostruisco la soluzione
        
        //Calcolo l'indice massimo dell'ultima colonna (non sta in backpointer)
        int best_lastclm_index = 0; //per backpoint
        double best_lastcml_prob = 0;
        
        for(int i = 0; i <= T; i++) { //tutti i tag
            double actual_prob = viterbi[i][N];
            if(actual_prob > best_lastcml_prob) {
                best_lastcml_prob = actual_prob;
                best_lastclm_index = i;
            }
        }   

        //La prima parola con tag!
        
        result.add(0, new WordTagCouple(phrase[N], tag_list.get(best_lastclm_index)));
        
        //NLPutil.MiscUtil.printMatrix(backpointer);
        
        int prev_i = best_lastclm_index;
        for(int i = N-1; i >= 0; i--) {
            result.add(0, new WordTagCouple(phrase[i], tag_list.get(backpointer[prev_i][i+1])));            
            prev_i = backpointer[prev_i][i+1];
        }
      
        
        
        //Ricosturisco la mia soluzione (popolando le varie WordTagCouple).
        
        //System.out.println(result);
        
        return result;
    }
    
    /**
     * Restituisce P(word | tag) se il dato è presente nella matrice di emissione.
     * In caso contrario applica smoothing a seconda del valore della variabile SMOOTH_TYPE.
     * 
     * @param tag
     * @param word
	 * @param tag_list
	 * @param emission_probability
     * @return 
     */
    public static Double smoothed_emission_probability(String tag, String word, ArrayList<String> tag_list, HashMap<String, HashMap<String, Double>> emission_probability) {
        
        
        if(emission_probability.get(tag).containsKey(word)) { //esiste la parola
            return emission_probability.get(tag).get(word);
        } else { //non ho w_i --> smoothing
//            System.out.println("faccio smoothing per: "+word);
            double ntag = tag_list.size();
            switch(SMOOTH_TYPE) {                    
                case "UNKNOWN" : 
                    return (double) 1.0/ntag;
                	
                case "MORPHIT" : 
                    return MorphitWrapper.getInstance().smoothingViterbi(tag, word, TAGSET_TYPE, ntag);				
                
                case "GOODTURING" : 
                    throw new UnsupportedOperationException("Tipo di smoothing ancora non supportato.");
                
                default: 
                    throw new IllegalArgumentException("Parametro di smoothing non esistente.");
            }
        }
    }


    
    
}
