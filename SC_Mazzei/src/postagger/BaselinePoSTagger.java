/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package postagger;

import static NLPutil.UDTPTaggingUtil.UDTP_WordTagCount;
import static NLPutil.UDTPTaggingUtil.UDTP_WordTagCouples;
import NLPutil.MorphitWrapper;
import NLPutil.WordTagCouple;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Enrico
 */
public class BaselinePoSTagger {
    
	
		/**
     * #################
     * #################
     * 
     * Variabili di esecuzione
     * 
     * #################
     * #################
     */	
	
    private static final String TAGGER_NAME = "Baseline";
    
    //Possibili normalizzazioni:
    //NONE --> nessuna normalizzazione
    //CAPITALIZE --> tutte le parole sono rese minuscole
	//LEMMING --> le parole sono ridotte alla forma normale	mediante Morphit
    private static final String NORMALIZATION_TYPE = "LEMMING";
    
    //Possibili tagset:
    //C4 --> tag presi dalla colonna 4
    //C5 --> tag presi dalla colonna 5 
    private static final String TAGSET_TYPE = "C5";
    
    private static final String method_name = TAGGER_NAME+ " - Tagset: " + TAGSET_TYPE + ", Normalization: "+NORMALIZATION_TYPE+", Smoothing: CONSTANT";
    
	
	/**
     * #################
     * #################
     * 
     * Files
     * 
     * #################
     * #################
     */	
	
    //Training set & test set
    private static final String train_set_path = "corpus/pos/it-universal-train.conll";
    private static final String test_set_path = "corpus/pos/it-universal-test.conll";
    

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
		// Struttura  che mantiene per ogni coppia parola w_i e tag t_i il numero di volte in cui w_i è stata classificata come t_i.
		HashMap<String, HashMap<String, Integer>> word_tag_count;

		// Struttura che mantiene per ogni parola il suo tag più frequentemente adottato.
		// Viene popolato in fase d'uso del tagger, per motivi di efficienza (non è necessario tutte le volte percorrere word_tag_count).
		HashMap<String, String> most_frequent_tag = new HashMap<>();			
			
			
        
        //Inizializziamo morphit prima di tutto così da non perdere tempo nell'algoritmo			
		if(NORMALIZATION_TYPE.equals("LEMMING")) MorphitWrapper.getInstance();		
			
        //Popolo la struttura base
        word_tag_count = UDTP_WordTagCount(train_set_path, TAGSET_TYPE, NORMALIZATION_TYPE);
        
        //Lettura del test set
        ArrayList<ArrayList<WordTagCouple>> test_set = UDTP_WordTagCouples(test_set_path, TAGSET_TYPE, NORMALIZATION_TYPE);
        
        //Applicazione dell'algoritmo (frase per frase) 
        ArrayList<ArrayList<WordTagCouple>> baseline_result = new ArrayList<>();
        long baseline_elapsed_time = 0;
        for (ArrayList<WordTagCouple> one_phrase_al : test_set) {
			//Applicazione dell'algoritmo di baseline
			long startTime = System.currentTimeMillis();
			ArrayList<WordTagCouple> one_baseline_result = baselinePoSTagger(one_phrase_al, most_frequent_tag, word_tag_count);
			baseline_elapsed_time += System.currentTimeMillis()-startTime;

			baseline_result.add(one_baseline_result); //aggiungo al risultato finale per fare evaluation                
        }
		
      
        NLPutil.EvaluateUtil.printEvaluationForMethod(method_name, baseline_result, test_set, ((double) baseline_elapsed_time/1000)+"");
      
        } catch(Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
    
    /**
     * Baseline PoS tagger: ad ogni parola è assegnato il suo tag più frequente nel corpus.
     * Lo smoothing consiste nell'assegnare il tag NOUN.
     * 
     * Il parametro test_set contiene già le risposte al tag, 
     * ma viene usato solo per inizializzare in maniera consistente il risultato.
     * 
     * @param test_set
	 * @param most_frequent_tag
	 * @param word_tag_count
     * @return 
     */
    public static  ArrayList<WordTagCouple>  baselinePoSTagger(ArrayList<WordTagCouple> test_set, 
																HashMap<String, String> most_frequent_tag, 
																HashMap<String, HashMap<String, Integer>> word_tag_count) {
        ArrayList<WordTagCouple> tag_result = new ArrayList<>();
                
        for(int i = 0; i < test_set.size(); i++) {
            String word = test_set.get(i).getWord();
            String best_tag = ""; 
            
            
            //Prima di tutto creiamo l'istanza nel tag_result (inizializzo con la parola)
            WordTagCouple one_couple = new WordTagCouple(word);

            if(most_frequent_tag.containsKey(word)) { //ho già trovato questa parola, posso usare il best che ho già calcolato precedentemente 
                best_tag = most_frequent_tag.get(word);
            } else { //non ho mai incontrato questa parola: devo scoprire il tag migliore
                
                if(word_tag_count.containsKey(word)) { //non è necessario smoothing, abbiamo la parola nel nostro DB
                    
                    HashMap<String, Integer> tags_for_word = word_tag_count.get(word); //tutti i tag per la parola (con count associati)

                    int best_count = -1;

                    for (HashMap.Entry<String, Integer> entry : tags_for_word.entrySet()) { //becco la migliore
                        String tag_i = entry.getKey();
                        Integer count_i = entry.getValue();

                        if(count_i > best_count) { //ho trovato un tag migliore 
                            best_count = count_i;
                            best_tag = tag_i;
                        }
                    }
                }
                else { //Non ho la parola, smoothing
                    best_tag = "NOUN"; 
                }
                
                //me la salvo per dopo (ottimizzazione banale)
                most_frequent_tag.put(word, best_tag); 
            }

            one_couple.setTag(best_tag);
            tag_result.add(one_couple);

        }
               
        return tag_result;
    }
    
}
