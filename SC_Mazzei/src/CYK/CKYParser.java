package CYK;

//import NLPutil.PCFGUtil;

import NLPutil.EvaluateUtil;
import static NLPutil.MiscUtil.printStringAsFile;
import NLPutil.MorphitWrapper;
import NLPutil.PCFGUtil.*;
import NLPutil.TUTUtil;
import static NLPutil.TUTUtil.elaborateTUT_py;
import static NLPutil.TUTUtil.extractTestSet;
import static NLPutil.TUTUtil.parseProbabilisticGrammar;
import NLPutil.UDTPTaggingUtil;
import static NLPutil.UDTPTaggingUtil.UDTP_TransitionEmissionProbability;
import static NLPutil.UDTPTaggingUtil.initHashMapCounters;
import NLPutil.WordTagCouple;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import static postagger.ViterbiPoSTagger.viterbiPoSTagger;



/**
 *
 * @author Enrico
 */
public class CKYParser {

	
	/**
     * #################
     * #################
     * 
     * Variabili di esecuzione
     * 
     * #################
     * #################
     */	
	
	static final boolean  REGEN_FILES = false; 	//Se true, vengono sempre rigenerati i files dal corpus
	static final boolean  ONLY_EVAL = false; 	//Se true, non viene eseguito cky ma solo la valutazione
	static final boolean  ENABLE_POS_TAGGING = true; //se true, si aggiunge il pos tagging mediante algoritmo di viterbi 

	
	
	/**
     * #################
     * #################
     * 
     * Files
     * 
     * #################
     * #################
     */
	
	private static final String POS_MODE_SUFFIX = (ENABLE_POS_TAGGING? "_4PoS":"");
	
	//Tutti i path dei file usati. L'unico file pre-esistente deve essere tut_dirty_path, gli altri saranno generati da python.
	//Java userà solo grammar_path, test_set_CNFtree_path, test_set_flatten_path
	private static final String tut_dirty_path  = "corpus/cky/tut-dirty.penn"; //il corpus (sporco)
	private static final String tut_clean_train_path = "corpus/cky/tut-clean-train"+POS_MODE_SUFFIX+".penn"; //il 90% del corpus (pulito), usato per il training
    private static final String tut_clean_test_path = "corpus/cky/tut-clean-test"+POS_MODE_SUFFIX+".penn"; //il 10% del corpus (pulito), usato per il testing (gold standard)
	private static final String grammar_path = "corpus/cky/PCFG/tut-grammar"+POS_MODE_SUFFIX+".pcfg"; //file che contiene la grammatica estratta dal training set
    private static final String test_set_CNFtree_path = "corpus/cky/tut-clean-test-CNFtree"+POS_MODE_SUFFIX+".penn"; //file che contiene una serie di alberi (di test)
	private static final String test_set_flatten_path = "corpus/cky/tut-clean-test-flatten.penn"; //file che contiene tutte le foglie (frasi), una per riga
	
	//file che contiene una lista di parole con colonne, in formato identico al train set di UDTP così da poter riusare il codice di quella classe
	private static final String train_set_CNFcolumns_4PoS_path = "corpus/cky/tut-clean-train-CNFcolumns"+POS_MODE_SUFFIX+".penn"; 
	
	//Contiene la soluzione del parse
	private static final String test_set_cky_parse_solution =  "results/computation/cky/test_set_cky_parse_solution"+POS_MODE_SUFFIX+".txt";
	
	public static void main(String[] args) {
	
		// ### Dichiarazioni variabili 
		PGrammar grammar;		
		
		try {
		
			System.out.println("Execution mode:\n");
			System.out.println(" - Python files regeneration: "+(REGEN_FILES? "enabled":"disabled"));
			System.out.println(" - Only evaluation (no CKY execution): "+(ONLY_EVAL? "enabled":"disabled"));
			System.out.println(" - External PoS Tagging (viterbi): "+(ENABLE_POS_TAGGING? "enabled":"disabled"));
			System.out.println("---------------------------------------------\n\n");
			
			//Per lo smoothing
			System.out.println("* Loading Morph-it!...\n");			
			MorphitWrapper.getInstance();	
			
						
			//Metodo che, grazie a python, genera file di testo pre-processati 
			//e pronti per essere trattati da Java durante l'esecuzione
			
			File probe_file = new File(tut_clean_test_path);
			if(!probe_file.exists() || REGEN_FILES) { //mando in esecuzione solo se non li ho già generati
				System.out.println("* Executing python script...");
				elaborateTUT_py(tut_dirty_path, tut_clean_train_path, tut_clean_test_path, grammar_path, test_set_CNFtree_path, test_set_flatten_path, ENABLE_POS_TAGGING, train_set_CNFcolumns_4PoS_path); 
			}
			
			/* ##### PARSIFICO LA GRAMMATICA ##### */
			System.out.println("* Obtaining probabilistic grammar...\n");			
			grammar = parseProbabilisticGrammar(grammar_path);
			
			
			//Lo estraggo prima perché potrebbe servire al pos tagging se è attivo
			ArrayList<ArrayList<String>> test_set = extractTestSet(test_set_flatten_path);
			
			
			/* ##### VITERBI POS TAGGING ##### */
			if(ENABLE_POS_TAGGING) { 
				preprocess_ViterbiPoSTagging(test_set, grammar);
			}
				
			//System.exit(0);
			/* ##### CKY ##### */			
			if(!ONLY_EVAL) {//Eseguiamo CKY solo se questo flag è attivo	
				System.out.println("* Executing CKY parser...\n");
				System.out.print("\t Completed sentences: ");
				
				 //Applicazione dell'algoritmo (frase per frase) 
				String cky_parse_solution = "";
				long cky_elapsed_time = 0;
				for (ArrayList<String> one_phrase_al : test_set) {
					//System.out.println("# Executing CKY on the sentence:\n'"+MiscUtil.printSentence(one_phrase_al)+"'\n");
					System.out.print("|");
					//Applicazione dell'algoritmo CKY
					long startTime = System.currentTimeMillis();
					String one_tree = cky(one_phrase_al, grammar);
					cky_elapsed_time += System.currentTimeMillis()-startTime;

//						pprintSExpression(one_tree);
//						System.out.println(one_tree);

					cky_parse_solution += one_tree+"\n"; //aggiungo al risultato finale per fare evaluation                
				}
				System.out.println("\n\nTempo trascorso: "+((double)cky_elapsed_time/1000)+" secondi.\n\n");

				printStringAsFile(test_set_cky_parse_solution, cky_parse_solution);
			}
			
			/* ##### EVAL ##### */			
			System.out.println("* Evaluating...\n");										
			EvaluateUtil.printEvalB(test_set_cky_parse_solution, test_set_CNFtree_path, POS_MODE_SUFFIX);
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}


	/**
	 * Algoritmo di CKY.
	 * @param phrase
	 * @param grammar
	 * @return 
	 */
	public static String cky(ArrayList<String> phrase, PGrammar grammar) {
		//Per correttezza degli indici, metto uno spazio vuoto in testa alla frase
		phrase.add(0, "");
		
		//strutture
		HashMap<Nonterminal, Double>[][] table = initEmptyTable(phrase.size());
		HashMap<Nonterminal, BackTriple>[][] back = initEmptyBack(phrase.size());
		
		for(int j = 1; j < phrase.size(); j++) { //Formalismo, regole del tipo A -> phrase.get(j)
			Set<PProduction> list_of_productions = grammar.getProductionsByBody(new Terminal(phrase.get(j))); //produzioni che hanno A come testa e word come corpo (lessicalizzata)
			if(list_of_productions.isEmpty()) 
				list_of_productions = TUTUtil.smoothedProductionsForWord(phrase.get(j), ENABLE_POS_TAGGING);
			
			for (PProduction production : list_of_productions) { //una produzione
				Nonterminal A = production.getNonterminal(); //A, la testa
				table[j-1][j].put(A, production.getProbability());
			}
			
//			System.out.println(phrase.get(j));
//			MiscUtil.printMatrix(table);
//			System.out.println("\n\n");
//			
			for(int i = j-2; i >= 0; i--) {
				for(int k = i+1; k <= j-1; k++) {
					//vogliamo tutte le regole del tipo A -> BC tali che table[i,k,B] > 0 e table[k,j,C] > 0
					//pertanto prendiamo tutte le possibili B di table[i,k] e tutte le possibili C di table[k,j]; 
					// verifichiamo per quali accoppiate sono entrambi > 0 e poi per ogni coppia cerchiamo le regole del tipo A -> BC
					Set<Nonterminal> hm_ik = table[i][k].keySet(); //tutte le possibili B
					Set<Nonterminal> hm_kj = table[k][j].keySet(); //tutte le possibili C
					for (Nonterminal B : hm_ik) {	
						for(Nonterminal C : hm_kj) {
							if(table[i][k].get(B) > 0 && table[k][j].get(C) > 0) { //prendiamo le teste
								list_of_productions = grammar.getProductionsByBody(B,C); //produzioni che hanno A come testa e BC come corpo (non lessicalizzate) (e le altre condizioni delle probabilità su table)
								
								for (PProduction production : list_of_productions) { //una produzione
									Nonterminal A = production.getNonterminal(); //A, la testa
									
									double prob_ikB = table[i][k].get(B); 
									double prob_kjC = table[k][j].get(C);
									double this_production_prob = production.getProbability();
									double new_probability = this_production_prob * prob_ikB * prob_kjC; //proposta di nuova probabilità: produrre parole dalla i alla j con la regola A -> BC
									
									if(!table[i][j].containsKey(A) || (table[i][j].get(A) < new_probability)) { 
										table[i][j].put(A, new_probability);
										back[i][j].put(A, new BackTriple(k, B, C));
									}
								}
							}
						}
						
					}			
				}
			}
				
		}
		
//		MiscUtil.printMatrix(table);
		
		//Trattiamo a parte tutte le produzioni del tipo H -> B. Alla creazione della grammatica, queste regole hanno assegnate un C = '_PLACEHOLDER_'. 
		//Gli unici B che prendiamo in considerazione sono quelli presenti nella cella finale dell'algoritmo, ossia quei B in grado di produrre l'intera sentence 
		//(dalla testa H andiamo direttamente in B quindi è una riscrittura).
		//In definitiva si tratta di un caso particolare delle regole A -> BC. Le assunzioni sono quindi che la testa (A) sia H, e che C sia '_PLACEHOLDER_'.
		//L'obbiettivo è individuare la migliore B.
		HashMap<Nonterminal, Double>  table_final_cell = table[0][phrase.size()-1];
		Set<Nonterminal> final_Bs = table_final_cell.keySet(); //tutte le possibili B dell'ultima cella
		
		//Cerco il miglior B
		double max_prob = 0;
		Nonterminal best_B = new Nonterminal("S"); //inizializzazione a caso
		for (Nonterminal B : final_Bs) {	
			Nonterminal C = new Nonterminal("_PLACEHOLDER_");
			if(table_final_cell.get(B) > 0) { //prendiamo le teste
				Set<PProduction> list_of_productions = grammar.getProductionsByBody(B,C); 

				for (PProduction production : list_of_productions) { //una produzione
					//Per costruzione la testa sarà necessariamente H, poiché le uniche regole con C = '_PLACEHOLDER_' come corpo sono quelle con testa H.						

					double prob_final_B = table_final_cell.get(B); 
					double this_production_prob = production.getProbability();
					double new_probability = this_production_prob * prob_final_B; //proposta di nuova probabilità: produrre parole dalla i alla j con la regola A -> BC

					if(new_probability > max_prob) { 
						max_prob = new_probability;
						best_B = B;
					}
				}
			}
		}
		
		return "(H ("+buildTree(phrase, back, 0, phrase.size()-1, best_B)+"))";
	}
	
	/**
	 * Ricostruisce la soluzione in forma testuale, ossia una S-espressione che rappresenta l'albero ottenuto da CYK.
	 *
	 * @param back
	 * @param i
	 * @param j
	 * @param A
	 * @param phrase 
	 * @return 
	 */
	public static String buildTree(ArrayList<String> phrase, HashMap<Nonterminal, BackTriple>[][] back, int i, int j, Nonterminal A) {
		if(back[i][j].containsKey(A)) {
			BackTriple bt = back[i][j].get(A); //ci restituisce {k,B,C}
			int k = bt.getIndex();
			Nonterminal B = bt.getFirstNonterminal();
			Nonterminal C = bt.getSecondNonterminal();

			return A.getName()+ "("+buildTree(phrase, back, i, k, B)+")"+ "("+buildTree(phrase, back, k, j, C)+")";

		}
		else {
			return A.getName()+" "+phrase.get(j);
		}
	}
	
	
	
		/**
	 * Metodo che effettua PoS tagging mediante Viterbi e con i suoi risultati popola delle produzioni 
	 * (lessicalizzate) che vengono poi aggiunte alla grammatica.
	 * 
	 * Per maggiori informazioni in merito al contenuto, consultare ViterbiPoSTagger.java
	 * 
	 * @param test_set
	 * @param grammar
	 */
	public static void preprocess_ViterbiPoSTagging(ArrayList<ArrayList<String>> test_set, PGrammar grammar) {
		try {
			System.out.println("* Training and executing PoS tagging...\n");							

			//Inizializzo la tag list
			ArrayList<String> tag_list = UDTPTaggingUtil.UDTP_ExtractTagList(train_set_CNFcolumns_4PoS_path, "C4");

//			System.out.println(tag_list);
			
			//Popolo la struttura base (training)
			ArrayList<HashMap<String, HashMap<String, Double>>> structures = UDTP_TransitionEmissionProbability(train_set_CNFcolumns_4PoS_path, tag_list, "C4", "CAPITALIZE");
			HashMap<String, HashMap<String, Double>> transition_probability = structures.get(0);
			HashMap<String, HashMap<String, Double>> emission_probability = structures.get(1);

			//Eseguo viterbi
			ArrayList<ArrayList<WordTagCouple>> viterbi_result = new ArrayList<>();

			for (ArrayList<String> one_phrase_al : test_set) {
				//converto l'arraylist in String[] 
				String[] one_phrase = one_phrase_al.toArray(new String[one_phrase_al.size()]);

				//Applicazione dell'algoritmo di viterbi
				ArrayList<WordTagCouple> one_viterbi_result = viterbiPoSTagger(one_phrase, tag_list, transition_probability, emission_probability);

				viterbi_result.add(one_viterbi_result); //aggiungo al risultato 

			}				

			//Il risultato di viterbi viene integrato nella grammatica (calcolo della prob!)
			//aver assegnato il tag A al termine t significa aggiungere un'occorrenza della regola A -> t. 
			//la probabilità per quella regola sarà poi calcolata come il conteggio delle A -> t diviso il numero di volte in cui il tag A è stato assegnato (occorrenze di A).
			//quindi faccio un for sulla soluzione di viterbi, popolo le due hashmaps (count regole, da scrivere dinamicamente e count tag) 
			//e poi percorro quella delle regole creando produzioni come la chiave della map e probabilità il valore della map divis oil valore corrispondente nella hashmap dei count tag.
			HashMap<String, Double> productions_occ_counter = new HashMap<>();
			HashMap<String, Double> tag_count = initHashMapCounters(tag_list);		
			
			for (ArrayList<WordTagCouple> one_viterbi_phrase : viterbi_result) {
				for (WordTagCouple one_viterbi_wordtagcouple : one_viterbi_phrase) {
						String tag = one_viterbi_wordtagcouple.getTag();
						String word = one_viterbi_wordtagcouple.getWord();
					
						String prod_string_key = tag +"->" + word; //poi splittiamo per freccia
						
						//conteggio l'occorrenza del tag
			            tag_count.put(tag, tag_count.get(tag)+1);
						
						if(productions_occ_counter.containsKey(prod_string_key)) { //esiste la produzione 
							productions_occ_counter.put(prod_string_key, productions_occ_counter.get(prod_string_key)+1); //incremento il count
						} else { //non ho mai incontrato la produzione, la aggiungo 
							productions_occ_counter.put(prod_string_key, 1.0); //inzializzo ad 1
						}
				}
			}
			
			//Abbiamo formalizzato le produzioni ed abbiamo i conteggi. Siamo pronti a creare le produzioni.
			Collection<PProduction> production_coll = new ArrayList<>(); //accumulatore di produzioni
			
			for (Map.Entry<String, Double> one_production_string : productions_occ_counter.entrySet()) {
				String key = one_production_string.getKey();
				String tag = key.split("->")[0];
				String word = key.split("->")[1];
				
				Double count = one_production_string.getValue();
				Double tag_occ = tag_count.get(tag);
				
				//Creo la produzione
				Nonterminal head = new Nonterminal(tag);
				Terminal t = new Terminal(word);
				
				production_coll.add(new PProduction(head, new ArrayList<Symbol>(){{add(t);}}, count/tag_occ));				
				
			}
				
			grammar.addProductions(production_coll);
									
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * Metodo di utilità che inizializza tutte le HashMaps di table.
	 * @param length
	 * @return 
	 */
	public static HashMap<Nonterminal, Double>[][] initEmptyTable(int length) {
		HashMap<Nonterminal, Double>[][] table = new HashMap[length][length];
		
		for(int i = 0; i < table.length; i++) {
			for(int j = 0; j < table[i].length; j++) {
				table[i][j] = new HashMap<>();
			}
		}
		
		return table;
	}
	
	/**
	 * Metodo di utilità che inizializza tutte le HashMaps di table.
	 * @param length
	 * @return 
	 */
	public static HashMap<Nonterminal, BackTriple>[][] initEmptyBack(int length) {
		HashMap<Nonterminal, BackTriple>[][] back = new HashMap[length][length];
		
		for(int i = 0; i < back.length; i++) {
			for(int j = 0; j < back[i].length; j++) {
				back[i][j] = new HashMap<>();
			}
		}
		
		return back;
	}
	

	/**
	 * Classe di supporto che memorizza una tripla contenuta nella struttura "back".
	 * 
	 */
	protected static final class BackTriple {
		
		private final int index;
		private final Nonterminal firstNonterminal;
		private final  Nonterminal secondNonterminal;	
		
		public BackTriple(int i, Nonterminal nt_1, Nonterminal nt_2) {
			index = i;
			firstNonterminal = nt_1;
			secondNonterminal = nt_2;
		}

		public int getIndex() {
			return index;
		}


		public Nonterminal getFirstNonterminal() {
			return firstNonterminal;
		}

		public Nonterminal getSecondNonterminal() {
			return secondNonterminal;
		}

		
	}
	
}
