/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NLPutil;

/**
 *
 * Classe che rappresenta una coppia parola-tag.
 * 
 * @author Enrico
 */
public class WordTagCouple {
    
    private final String word;

    private String tag;

    public WordTagCouple(String w, String t) {
        word = w;
        tag = t;
    }
    
    public WordTagCouple(String w) {
        word = w;
        tag = "";
    }
    
    public String getWord() {
        return word;
    }

    public String getTag() {
        return tag;
    }
   
    public void setTag(String t) {
        tag = t;
    }
    
    @Override
    public String toString() {
        return "["+this.getWord()+", "+this.getTag()+"]";
    }
}
