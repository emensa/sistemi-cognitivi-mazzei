/*
 * Author: Enrico Mensa (enrico.mensa@gmail.com) 
 */
package NLPutil.PCFGUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * A class representing a probabilistic context-free grammar.
 */
public final class PGrammar implements Iterable<PProduction> {
    /** A collection of the productions in the grammar. */
    private final Collection<PProduction> productions;

    /** A collection of the nonterminals in the grammar. */
    private final Set<Nonterminal> nonterminals = new HashSet<>();

    /** A collection of all the terminals in the grammar. */
    private final Set<Terminal> terminals = new HashSet<>();

    /** The start symbol. */
    private final Nonterminal start;

    /** A map associating nonterminals with their productions. */
    private Map<Nonterminal, Collection<PProduction>> productionsFor;
	
	/** A map associating entire productions with their body(non lexicalized). */
    private Map<List<Symbol>, Set<PProduction>> productionByBody_NLex;
	
	/** A map associating entire productions with their body(lexicalized). */
    private Map<Symbol, Set<PProduction>> productionByBody_Lex;	
	
	/** Epsilon di tolleranza per la verifica della consistent. */
	private final double epsilon_toll = 0.00001;

    /**
     * Constructs a new grammar from the given productions and start symbol.
     *
     * @param productions The productions in the grammar.
     * @param start The start symbol of the grammar.
     */
    public PGrammar(Collection<PProduction> productions, Nonterminal start) {
				
		
        if (productions == null || start == null)
            throw new NullPointerException();

        this.productions = productions;
        this.start = start;

        /* Fill in the list of terminals and nonterminals. */
        for (PProduction p: productions) {
            nonterminals.add(p.getNonterminal());
            for (Symbol s: p.getProduction()) {
                if (s instanceof Nonterminal)
                    nonterminals.add((Nonterminal) s);
                else // s instanceof Terminal
                    terminals.add((Terminal) s);
            }
        }

        /* Confirm that the start symbol is a nonterminal in the grammar. */
        nonterminals.add(start);

        /* Construct the map from nonterminals to productions. */
        productionsFor = constructProductionMap();
		
		/* Construct the map from body to productions (non lex). */
		productionByBody_NLex = constructProductionByBody_NLexMap();
		
		/* Construct the map from body to productions (lex). */
		productionByBody_Lex = constructProductionByBody_LexMap();		
		
		if(!isConsistent()) throw new IllegalArgumentException("The cumulative probability of each production having the same nonterminal isn't 1.0.");
		
		
		
    }

    /**
     * Constructs a map associating each nonterminal with the set of
     * productions it yields.  This makes it easier to determine what possible
     * productions we might need to explore in a given context.
     *
     * @return A map associating nonterminals with their productions.
     */
    private Map<Nonterminal, Collection<PProduction>> constructProductionMap() {
        /* Allocate space for the result. */
        Map<Nonterminal, Collection<PProduction>> result = 
            new HashMap<>();

        /* Create a new set for each nonterminal. */
        for (Nonterminal nt: getNonterminals())
            result.put(nt, new HashSet<>());

        /* Scan across the grammar filling all of these sets in. */
        for (PProduction prod: this)
            result.get(prod.getNonterminal()).add(prod);

        /* Update the map by giving back an immutable view of the entries. */
        for (Nonterminal nt: getNonterminals())
            result.put(nt, Collections.unmodifiableCollection(result.get(nt)));

        return result;
    }
	
	
	/**
     * Constructs a map associating each list of symbols (non lexicalized) with the set of
     * production that involve them. 
     *
     * @return
     */
    private Map<List<Symbol>, Set<PProduction>> constructProductionByBody_NLexMap() {
        /* Allocate space for the result. */
        Map<List<Symbol>, Set<PProduction>> result = new HashMap<>();

		
		for(PProduction prod : getProductions()) {
			if(!prod.isLexicalized()) { //ci interessano solo le non lessicalizzate
				if(!result.containsKey(prod.getProduction())) { //il corpo non è già nella map
					Set<PProduction> heads = new HashSet<>();
					heads.add(prod);
					result.put(prod.getProduction(), heads);
				} else { //corpo già nella map, aggiungo solo la testa appena trovata
					result.get(prod.getProduction()).add(prod);					
				}
			}
		}
        return result;
    }
	
	/**
     * Constructs a map associating each list of symbols (non lexicalized) with the set of
     * production that involve them. 
     *
     * @return 
     */
    private Map<Symbol, Set<PProduction>> constructProductionByBody_LexMap() {
        /* Allocate space for the result. */
        Map<Symbol, Set<PProduction>> result = new HashMap<>();

		
		for(PProduction prod : getProductions()) {
			if(prod.isLexicalized()) { //ci interessano solo le lessicalizzate
				if(!result.containsKey(prod.getProduction().get(0))) { //il corpo non è già nella map
					Set<PProduction> heads = new HashSet<>();
					heads.add(prod);
					result.put(prod.getProduction().get(0), heads);
				} else { //corpo già nella map, aggiungo solo la testa appena trovata
					result.get(prod.getProduction().get(0)).add(prod);					
				}
			}
		}

        return result;
    }	

    /**
     * Returns the start symbol of the grammar.
     *
     * @return The start symbol of the grammar.
     */
    public Nonterminal getStartSymbol() {
        return start;
    }

    /**
     * Returns an immutable view of the productions in the grammar.
     *
     * @return An immutable view of the productions in the grammar.
     */
    public Collection<PProduction> getProductions() {
        return Collections.unmodifiableCollection(productions);
    }

    /**
     * Returns an immutable iterator over the productions in the grammar.
     *
     * @return An immutable iterator over the productions in the grammar.
     */
	@Override
    public Iterator<PProduction> iterator() {
        return getProductions().iterator();
    }

    /**
     * Returns an immutable view of the terminals in the grammar.
     *
     * @return An immutable view of the terminals in the grammar.
     */
    public Collection<Terminal> getTerminals() {
        return Collections.unmodifiableCollection(terminals);
    }

    /**
     * Returns an immutable view of the nonterminals in the grammar.
     *
     * @return An immutable view of the nonterminals in the grammar.
     */
    public Collection<Nonterminal> getNonterminals() {
        return Collections.unmodifiableCollection(nonterminals);
    }

    /**
     * Given a nonterminal, returns an immutable view of the productions that
     * begin with that nonterminal.
     *
     * @param nt The nonterminal whose productions should be found.
     * @return A collection of the productions associated with that nonterminal.
     */
    public Collection<PProduction> getProductionsFor(Nonterminal nt) {
        Collection<PProduction> result = productionsFor.get(nt);
        if (result == null)
            throw new IllegalArgumentException("Nonterminal " + nt + " not found.");
        return result;
    }
	
	
	/**
     * Removes all the lexicalized productions in the grammar.
	 * 
	 * Pay attention: this method may make the grammar unconsistent. Use isConsistent() to verify that.
     *
     */
    public void removeLexicalizedProductions() {
		Collection<PProduction> lexicalized = new ArrayList<>();

		//Rimuoviamo le produzioni lessicalizzate
		for (PProduction one_production : productions) {
			if(one_production.isLexicalized()) lexicalized.add(one_production);
		}
		productions.removeAll(lexicalized);
    }
	
	/**
	 * Verifies that the grammar is consistent (all productions with same head must sum to 1).
	 * @return 
	 */
	public boolean isConsistent() {
		
		//Verifico che la somma delle produzioni con uguale testa sia 1
		for (Map.Entry<Nonterminal, Collection<PProduction>> entry : productionsFor.entrySet()) {
			Nonterminal head = entry.getKey();
			Collection<PProduction> production_list = entry.getValue();
			
			if(! production_list.isEmpty()) {
				double p_counter = 0.0;
				for (PProduction one_production : production_list) {
					p_counter += one_production.getProbability();
				}
				if(p_counter < (1.0-epsilon_toll) || p_counter > (1.0+epsilon_toll)) { 
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Dato un terminale restituisce la lista delle produzioni che lo hanno come corpo.
	 * Il metodo cerca quindi soltanto fra le regole di lessico.
	 * 
	 * In sostanza, cerca regole del tipo A -> word e le restituisce.
	 * 
	 * @param word
	 * @return 
	 */
	public Set<PProduction> getProductionsByBody(Terminal word) {
		
		
		return productionByBody_Lex.get(word) != null? productionByBody_Lex.get(word) : new HashSet<>();
	}
	
	/**
	 * Dati due nonterminali restituisce la lista delle produzioni che li hanno come corpo.
	 * 
	 * In sostanza, cerca regole del tipo A -> BC e le restituisce.
	 * 
	 * @param B
	 * @param C
	 * @return 
	 */
	public Set<PProduction> getProductionsByBody(Nonterminal B, Nonterminal C) {
		List<Symbol> body = new LinkedList<>();
		body.add(B);
		body.add(C);
		
		Set<PProduction> result = productionByBody_NLex.get(body);
		
		
		return result != null? result : new HashSet<>();
	}
	
	/**
	 * Integra all'interno della grammatica una serie di produzioni.
	 * @param production_coll 
	 */
	public void addProductions(Collection<PProduction> production_coll) {
		
		if (production_coll == null)
            throw new NullPointerException();

        /* Fill in the list of terminals and nonterminals. */
        for (PProduction p: production_coll) {
            nonterminals.add(p.getNonterminal());
            for (Symbol s: p.getProduction()) {
                if (s instanceof Nonterminal)
                    nonterminals.add((Nonterminal) s);
                else // s instanceof Terminal
                    terminals.add((Terminal) s);
            }
			productions.add(p); //aggiungo la produzione			
        }

        /* Construct the map from nonterminals to productions. */
        productionsFor = constructProductionMap();
		
		/* Construct the map from body to productions (non lex). */
		productionByBody_NLex = constructProductionByBody_NLexMap();
		
		/* Construct the map from body to productions (lex). */
		productionByBody_Lex = constructProductionByBody_LexMap();	
		
		if(!isConsistent()) throw new IllegalArgumentException("The cumulative probability of each production having the same nonterminal isn't 1.0.");
	}
	
	
    /**
     * Returns a human-readable description of the grammar.
     *
     * @return A human-readable description of the grammar.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Production p: this) {
            builder.append(p.toString());
            builder.append("\n");
        }
        return builder.toString();
    }
	
	
	/**
	 * Metodo di utilità per testare grammatiche probabilistiche.
	 */
	public static void testPCFG(){
		Nonterminal s1_S = new Nonterminal("S");
		Nonterminal s2_A = new Nonterminal("A");
		Nonterminal s3_B = new Nonterminal("B");
		Terminal s4_a = new Terminal("a");
		Terminal s5_b = new Terminal("b");

		List<Symbol> corpo_p1 = new ArrayList<>();
		corpo_p1.add(s2_A);
		corpo_p1.add(s3_B);		
		PProduction p1 = new PProduction(s1_S, corpo_p1, 0.3);

		List<Symbol> corpo_p2 = new ArrayList<>();
		corpo_p2.add(s4_a);
		PProduction p2 = new PProduction(s2_A, corpo_p2, 1.0);		

		List<Symbol> corpo_p3 = new ArrayList<>();
		corpo_p3.add(s5_b);
		PProduction p3 = new PProduction(s3_B, corpo_p3, 1.0);		

		ArrayList<PProduction> productions = new ArrayList<>();

		productions.add(p1);
		productions.add(p2);
		productions.add(p3);
		
		List<Symbol> corpo_p4 = new ArrayList<>();
		corpo_p4.add(s4_a);
		PProduction p4 = new PProduction(s1_S, corpo_p4, 0.7);		

		productions.add(p4);

		PGrammar grm = new PGrammar(productions, s1_S);

		System.out.println(grm);	
		
		Collection<PProduction> productionsFor = grm.getProductionsFor(s1_S);
		//System.out.println(productionsFor);
		
		grm.removeLexicalizedProductions();
				System.out.println(grm);
				System.out.println(grm.isConsistent());

		
	}
	
	public  void testPCFG2(){
		Set<Nonterminal> unique_heads = new HashSet<>();
		for(PProduction prod : getProductions()) {
			if(prod.isLexicalized()) { //ci interessano solo le lessicalizzate
				unique_heads.add(prod.getNonterminal());
			}
		}

		System.out.println(unique_heads);
		
	}


}