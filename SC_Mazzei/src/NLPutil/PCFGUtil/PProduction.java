/*
 * Estensione di Production per supportare le probabilità associate alle produzioni.
 * Author: Enrico Mensa (enrico.mensa@gmail.com) 
 */
package NLPutil.PCFGUtil;

import java.util.List;

/*
 * Estensione di PProduction per supportare le probabilità associate alle produzioni.
 * Author: Enrico Mensa (enrico.mensa@gmail.com) 
 */
public final class PProduction extends Production { 

	//Probabilità della produzione
	private final double probability;
	
	public PProduction(Nonterminal nt, List<Symbol> production, double probability) {
		super(nt, production);
		this.probability = probability;
	}
	
	/**
     * Returns the probability associated with the production.
     *
     * @return The probability associated with the production.
     */
    public double getProbability() {
        return probability;
    }
	
	/**
	 * Tells if the production is lexicalised (meaning that has only terminals in his production).
	 *
	 * @return a boolean that tells if the production is lexicalised.
	 */
	public boolean isLexicalized() {
		List<Symbol> theproduction = this.getProduction();
		for (Symbol symbol : theproduction) {
			if(!symbol.isTerminal()) return false;
		}
		return true;
	}
	
	/**
     * Returns a human-readable representation of this production.
     *
     * @return A human-readable representation of this production.
     */
    @Override 
    public String toString() {
        return this.getNonterminal() + " -> " + this.getProduction() + " [" + probability+"]";
    }
}
