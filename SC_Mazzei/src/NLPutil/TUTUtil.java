/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NLPutil;

import NLPutil.PCFGUtil.Nonterminal;
import NLPutil.PCFGUtil.PGrammar;
import NLPutil.PCFGUtil.PProduction;
import NLPutil.PCFGUtil.Symbol;
import NLPutil.PCFGUtil.Terminal;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Enrico
 */
public class TUTUtil {
	
	
	static final String elaborateTUT_python_path = "python/elaborateTUT.py";
	
	/**
	 * Estrae da file una grammatica probabilistica.
	 * @param file_path
	 * @return 
	 * @throws java.io.FileNotFoundException 
	 */
	public static PGrammar parseProbabilisticGrammar(String file_path) throws FileNotFoundException, IOException {
        
        //BufferedReader br = new BufferedReader(new FileReader(file_path));
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file_path), "UTF-8"));

		Collection<PProduction> production_coll = new ArrayList<>(); //accumulatore di produzioni
		Nonterminal start_symbol; //start symbol
                
        String line = br.readLine(); //la prima linea la leggo a parte, ha informazioni in merito alla grammatica
		String[] splitted_first_line = line.split(" ");
		int n_production = Integer.parseInt(splitted_first_line[2]); 
		start_symbol = new Nonterminal(splitted_first_line[7].substring(0,1));

        while((line = br.readLine()) != null) {
			line = line.substring(4); //rimuovo i 4 spazi iniziali

			String [] splitted_line = line.split(" ");
			
			Nonterminal head = new Nonterminal(splitted_line[0]); //la prima è sempre un non terminale (la head)
			boolean isHeadProduction = head.equals(start_symbol); //è una regola del tipo H -> S (ossia senza il secondo nonterminale)
			boolean isLexicalized = (splitted_line.length == 4) && !isHeadProduction;

			if(isLexicalized) { // è lessicalizzata, quindi raccatto solo un terminale
				Terminal t1 = new Terminal(removeFirstAndLastChar(splitted_line[2]));
				Double prob = new Double(removeFirstAndLastChar(splitted_line[3]));

				//aggiungo la produzione
				production_coll.add(new PProduction(head, new ArrayList<Symbol>(){{add(t1);}}, prob));
			} else { //non è lessicalizzato 
				Nonterminal nt1 = new Nonterminal(splitted_line[2]);
				Nonterminal nt2;
				Double prob;

				//ci sono alcune regole con testa H (il top della frase). Le elaboro a parte con un nonterminale _PLACEHOLDER_ associato (non hanno il C)	
				if(isHeadProduction) {
					nt2 = new Nonterminal("_PLACEHOLDER_");
					prob = new Double(removeFirstAndLastChar(splitted_line[3]));		

				} else {
					nt2 = new Nonterminal(splitted_line[3]);	
					prob = new Double(removeFirstAndLastChar(splitted_line[4]));
				}


				//aggiungo la produzione
				production_coll.add(new PProduction(head, new ArrayList<Symbol>(){{add(nt1); add(nt2);}}, prob));				
			}
		}
				            
        br.close();
		
        return new PGrammar(production_coll, start_symbol);		
	}
	
	/**
	 * Estrae da file le frasi del testset.
	 * 
	 * @param testset_path
	 * @return 
	 */
	public static ArrayList<ArrayList<String>> extractTestSet(String testset_path) {
		ArrayList<ArrayList<String>> result = new ArrayList<>();
		ArrayList<String> one_phrase;

			
		try { 
			BufferedReader brl = new BufferedReader(new InputStreamReader(new FileInputStream(testset_path), "UTF-8"));

			String line = "";
			//per ogni riga nel testset
			while((line = brl.readLine()) != null) {
				one_phrase = new ArrayList<>();
				
				String [] splitted_line = line.split(" ");
				for (String word : splitted_line) {
					one_phrase.add(word);
				}
				result.add(one_phrase);
			}
			brl.close();
			
		} catch (Exception ex ){
			ex.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 
	 * Metodo che legge il corpus sporco (primo parametro) e produce tutti gli altri file (altri parametri).
	 * Viene eseguito il file corpus/cyk/elaborateTUT.py
	 * 
	 * @param tut_dirty_path il corpus (sporco)
	 * @param tut_clean_train_path il 90% del corpus (pulito), usato per il training
	 * @param tut_clean_test_path il 10% del corpus (pulito), usato per il testing (gold standard)
	 * @param grammar_path file che contiene la grammatica estratta dal training set
	 * @param test_set_CNFtree_path file che contiene una serie di alberi (di test)
	 * @param test_set_flatten_path file che contiene tutte le foglie (frasi), una per riga
	 * @param ENABLE_POS_TAGGING
	 * @param train_set_CNF_4PoSmode_path file che contiene una versione a colonna del train set (per pos tagging)
	 */
	public static void elaborateTUT_py(String tut_dirty_path, 
											String tut_clean_train_path, 
											String tut_clean_test_path, 
											String grammar_path, 
											String test_set_CNFtree_path, 
											String test_set_flatten_path,
											boolean ENABLE_POS_TAGGING,
											String train_set_CNF_4PoSmode_path) {
		try {
			//Process p = Runtime.getRuntime().exec("python "+elaborateTUT_py.getAbsolutePath()+" "+tut_dirty_path+" "+tut_clean_train_path+" "+tut_clean_test_path+" "+grammar_path+" "+test_set_CNFtree_path+" "+test_set_flatten_path);
			String command = "python "+elaborateTUT_python_path+""+" "+tut_dirty_path+" "+tut_clean_train_path+" "+tut_clean_test_path+" "+grammar_path+" "+test_set_CNFtree_path+" "+test_set_flatten_path;
			if(ENABLE_POS_TAGGING) 
				command+=" -extpos "+train_set_CNF_4PoSmode_path;
			
			Process p = Runtime.getRuntime().exec(command);
			
			//System.out.println("python "+elaborateTUT_python_path+""+" "+tut_dirty_path+" "+tut_clean_train_path+" "+tut_clean_test_path+" "+grammar_path+" "+test_set_CNFtree_path+" "+test_set_flatten_path+ " "+(ENABLE_POS_TAGGING? "-extpos":""));			
			
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String one_line;
			String result = "";
			
			// leggo il responso di
			while ((one_line = in.readLine()) != null) {
				 result += one_line+"\n";
			}		
			System.out.println(result);			
			
		}catch(IOException | NumberFormatException e){
			e.printStackTrace();
		}
		
		
	}	
	
	/**
	 * Grazie all'ausilio di morphit, viene ritornata una lista di produzioni con probabilità associata per una parola sconosciuta.
	 * @param word
	 * @param ENABLE_POS_TAGGING
	 * @return 
	 */
	public static Set<PProduction> smoothedProductionsForWord(String word, boolean ENABLE_POS_TAGGING) {
		Set<PProduction> result = new HashSet<>();
		ArrayList<String[]> tag_prob_pairs = MorphitWrapper.getInstance().smoothingCky(word); //coppie tag-probabilità da cui costruire le produzioni. i tag sono di Google !
		Terminal terminal = new Terminal(word); //tutte le produzioni avranno questa parola come terminale
		
		for (String[] tag_prob_pair : tag_prob_pairs) { //un google tag
			String tag = tag_prob_pair[0];
			double prob = new Double(tag_prob_pair[1]);
						
			
			ArrayList<String> traslated_tags;
					
			//La traduzione è diversa a seconda dei tag in uso
			if(ENABLE_POS_TAGGING) traslated_tags = translateGooglePoS2CNFGooglePoS(tag);
			else traslated_tags = translateGooglePoS2CNFTUTPoS(tag);
						
			
			for (String one_translated_tag : traslated_tags) {
					tag = one_translated_tag;
					Nonterminal head = new Nonterminal(tag);

					//aggiungo la produzione
					result.add(new PProduction(head, new ArrayList<Symbol>(){{add(terminal); }}, prob/traslated_tags.size()));			

				}	
		}
									
		return result;
	}
	
	
	
	/**
	 * Traduce un tag di Google in una lista di quelli di TUT.
	 * Attenzone: i tag di TUT prodotti sono quelli in CNF (assunzione forte).
	 * @param pos
	 * @return 
	 */
	public static ArrayList<String> translateGooglePoS2CNFTUTPoS(String pos) {
	   ArrayList<String> tutpos = new ArrayList<>();
	   switch(pos) {
		   case "NOUN": 
				tutpos.add("NOU"); tutpos.add("S+VP+NP+NOU"); tutpos.add("NP+NOU"); 
				tutpos.add("PRN+NP+NOU"); tutpos.add("NP+NP+NOU");
				break;
		   case "ADJ": 
				tutpos.add("ADJ"); tutpos.add("S+VP+ADJP+ADJ"); tutpos.add("ADJP+ADJ"); 
				tutpos.add("VP+ADJP+ADJ"); tutpos.add("NP+ADJ");
				break;
		   case "VERB": 
				tutpos.add("VAU"); tutpos.add("VP+VAU"); tutpos.add("VP+VP+VMA"); 
				tutpos.add("VMA");tutpos.add("S+S+VP+VMA");tutpos.add("S+VP+VMA"); 
				tutpos.add("VP+VMA"); tutpos.add("VMO"); tutpos.add("SBAR+S+VP+VMA"); 
				tutpos.add("PRN+S+VP+VMA"); tutpos.add("NP+S+VP+VMA"); tutpos.add("VP+VMO"); 
				tutpos.add("S+VP+Vbar+VMO");
				break;
		   case "CONJ": 
				tutpos.add("CONJ");
				break;
		   case "PRON": 
				tutpos.add("PRO"); tutpos.add("PRDT"); tutpos.add("NP+PRO"); 
				tutpos.add("NP-SBJ1+PRO"); tutpos.add("NP-SBJ6+PRO"); tutpos.add("NP+NP+PRO");
				break;
		   case "ADV": 
				tutpos.add("ADVB"); tutpos.add("ADVP+ADVbar+ADVB"); tutpos.add("ADVP+ADVB");
				break;
		   case "ADP": 
				tutpos.add("PREP"); tutpos.add("PP+PREP");
				break;
		   case "DET": 
				tutpos.add("ART"); tutpos.add("NP+ART");
				break;
		   case ".": 
				tutpos.add(":"); tutpos.add("."); tutpos.add(","); tutpos.add("PUNCT");
				break;
		   case "X": 
				tutpos.add("SPECIAL"); tutpos.add("NP+DATE");
				break;
		   case "NUM": 
				tutpos.add("NUMR"); tutpos.add("PRN+NUMR"); tutpos.add("NP+NUMR"); tutpos.add("PRN+NP+NUMR");
				break;
	   }
	   return tutpos;
	}
	
	
	/**
	 * Traduce un tag di Google in una lista di quelli di Google:
	 * Attenzone: i tag di Google prodotti sono quelli in CNF (assunzione forte).
	 * @param pos
	 * @return 
	 */
	public static ArrayList<String> translateGooglePoS2CNFGooglePoS(String pos) {
	   ArrayList<String> gposCNF = new ArrayList<>();
	   switch(pos) {
		   case "NOUN": 
				gposCNF.add("NOUN"); gposCNF.add("NP+NOUN"); gposCNF.add("NP+NP+NOUN");
				gposCNF.add("PRN+NP+NOUN"); gposCNF.add("S+VP+NP+NOUN");  
				break;
		   case "ADJ": 
				gposCNF.add("ADJ"); gposCNF.add("ADJP+ADJ"); gposCNF.add("NP+ADJ");
				gposCNF.add("S+VP+ADJP+ADJ"); gposCNF.add("VP+ADJP+ADJ"); 
				break;
		   case "VERB": 
				gposCNF.add("VERB"); gposCNF.add("VP+VERB"); gposCNF.add("NP+S+VP+VERB");
				gposCNF.add("S+VP+VERB"); gposCNF.add("PRN+S+VP+VERB");	gposCNF.add("SBAR+S+VP+VERB"); 
				gposCNF.add("S+S+VP+VERB");	gposCNF.add("VP+VP+VERB");	gposCNF.add("S+VP+Vbar+VERB");
				break;
		   case "CONJ": 
				gposCNF.add("CONJ");
				break;
		   case "PRON": 
				gposCNF.add("NP+PRON");	gposCNF.add("PRDT"); gposCNF.add("PRON");  
				gposCNF.add("NP-SBJ1+PRON"); gposCNF.add("NP-SBJ6+PRON"); gposCNF.add("NP+NP+PRON");
				break;
		   case "ADV": 
				gposCNF.add("ADVP+ADV"); gposCNF.add("ADV"); gposCNF.add("ADVP+ADVbar+ADV"); 
				break;
		   case "ADP": 
				gposCNF.add("ADP"); gposCNF.add("PP+ADP");
				break;
		   case "DET": 
				gposCNF.add("DET"); gposCNF.add("NP+DET");
				break;
		   case ".": 
				gposCNF.add(","); gposCNF.add(".");
				gposCNF.add(":");  gposCNF.add("PUNCT");
				break;
		   case "X": 
				gposCNF.add("SPECIAL"); gposCNF.add("NP+DATE");
				break;
		   case "NUM": 
				gposCNF.add("NP+NUM"); gposCNF.add("PRN+NP+NUM");
				gposCNF.add("NUM"); gposCNF.add("PRN+NUM");  
				break;
	   }
	   return gposCNF;
	}
	
	/**
	 * Traduce in tag di TUT in uno di Google.
	 * Possibili imprecisioni sul verbo, che ha più istanze nel TUT.
	 * @param pos
	 * @return 
	 */
	public static String translateTUTPoS2GooglePoS(String pos) {
		switch(pos) {
			case "NOU": return "NOUN";
			case "ADJ": return "ADJ";
			case "VAU":
			case "VCA":
			case "VMO":
			case "VMA": return "VERB";
			case "CONJ": return "CONJ";
			case "PRO": return "PRON";
			case "ADVB": return "ADV";
			case "PREP": return "ADP";
			case "ART": return "DET";
			case "PUNCT": return ".";
			case "SPECIAL": return "X";
			case "NUMR": return "NUM";
		}
		return "NOUN";
	}
	
	
	/**
	 * Rimuove il primo ed ultimo carattere di una stringa e la ritorna.
	 * 
	 * @param s
	 * @return 
	 */
	public static String removeFirstAndLastChar(String s) {
		return s.substring(1, s.length()-1).replace("'", "");
	}
	
	
}
