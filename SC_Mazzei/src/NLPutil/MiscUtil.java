/*
 * Utilità varie.
 */
package NLPutil;

import NLPutil.PCFGUtil.Nonterminal;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Enrico
 */
public class MiscUtil {
    
    /**
     * Stampa una matrice di interi.
     * @param m 
     */
    public static void printMatrix(int[][] m){
        try{
            int rows = m.length;
            int columns = m[0].length;
            String str = "|\t";

            for(int i=0;i<rows;i++){
                for(int j=0;j<columns;j++){
                    str += m[i][j] + "\t";
                }

                System.out.println(str + "|");
                str = "|\t";
            }

        }catch(Exception e){System.out.println("Matrix is empty!!");}
    }
    
    /**
     * Stampa una matrice di float.
     * 
     * @param m 
     */
    public static void printMatrix(float[][] m){
        try{
            int rows = m.length;
            int columns = m[0].length;
            String str = "|\t";

            for(int i=0;i<rows;i++){
                for(int j=0;j<columns;j++){
                    str += m[i][j] + "\t";
                }

                System.out.println(str + "|");
                str = "|\t";
            }

        }catch(Exception e){System.out.println("Matrix is empty!!");}
    }   
    
    /**
     * Stampa una matrice di double.
     * 
     * @param m 
     */
    public static void printMatrix(double[][] m){
        try{
            int rows = m.length;
            int columns = m[0].length;
            String str = "|\t";

            for(int i=0;i<rows;i++){
                for(int j=0;j<columns;j++){
                    str += m[i][j] + "\t";
                }

                System.out.println(str + "|");
                str = "|\t";
            }

        }catch(Exception e){System.out.println("Matrix is empty!!");}
    }      
	
    /**
     * Stampa una matrice di HashMaps.
     * 
     * @param m 
     */
    public static void printMatrix(HashMap<Nonterminal, Double>[][] m){
        try{
            int rows = m.length;
            int columns = m[0].length;
            String str = "|\t";

            for(int i=0;i<rows;i++){
                for(int j=0;j<columns;j++){
                    str += m[i][j] + "\t";
                }

                System.out.println(str + "|");
                str = "|\t";
            }

        }catch(Exception e){System.out.println("Matrix is empty!!");}
    } 	
	
	
	/**
	 * Stampa in formato leggibile (indentato correttamente) la S-Expression sexpr
	 * 
	 * @param sexpr 
	 */
	public static void pprintSExpression(String sexpr) {
		int ntab=0;
		for(int i = 0; i < sexpr.length(); i++) {
			if(sexpr.charAt(i)=='(') {
				System.out.println();
				for(int j=0; j<ntab; j++)
					System.out.print("\t");
				ntab++;
			}
			else if(sexpr.charAt(i)==')') {
				ntab--;
			}
			System.out.print(sexpr.charAt(i));
		}
		System.out.println();
	}
	
	public static String returnPSExpression(String sexpr) {
		String ret = "";
		int ntab=0;
		for(int i = 0; i < sexpr.length(); i++) {
			if(sexpr.charAt(i)=='(') {
				ret += "\n";
				for(int j=0; j<ntab; j++)
					ret += "\t";
				ntab++;
			}
			else if(sexpr.charAt(i)==')') {
				ntab--;
			}
			ret += sexpr.charAt(i);
		}
		ret += "\n";
		
		return ret;
	}
	
	/**
	 * Crea il file file_path con contenuto file_content.
	 *
	 * @param file_path
	 * @param file_content 
	 */
	public static void printStringAsFile(String file_path, String file_content) {
	    try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file_path, false)))) {
        
            out.print(file_content);
        
        }catch (IOException ex) {
            ex.printStackTrace();
        }
	}
    
	/**
	 * Stampa una frase passata come array di parole.
	 * @param sentence 
	 * @return  
	 */
	public static String printSentence(ArrayList<String> sentence) {
		String result = "";
		for(String word : sentence) {
			result += word +" ";
		}
		
		return result.substring(0, result.length()-1);
	}
	
}
