/*
 * Classe che compie alcuni confronti e valutazioni per le performances di vari strumenti NLP.
 */
package NLPutil;

import static NLPutil.MiscUtil.printStringAsFile;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

/**
 *
 * @author Enrico
 */
public class EvaluateUtil {
    
    //Cartella nella quale sono riportati i file di evaluation
    private static final String pos_result_folder = "results/evaluations/pos/";
	private static final String cky_result_folder = "results/evaluations/cky/";
        
    /**
     * Valuta le performances di un tagger confrontando il risultato di questo (tagger) con il corpus originale. 
     * La valutazione è fatta frase per frase (quindi abbiamo array di array di WordTagCouple).
     * 
     * Viene prodotto un file method_name.txt che contiene i dati relativi, i quali sono anche stampati a video.
     * 
     * @param method_name
     * @param tagger
     * @param corpus
     * @param elapsed_secs
     */
    public static void printEvaluationForMethod(String method_name, ArrayList<ArrayList<WordTagCouple>> tagger, ArrayList<ArrayList<WordTagCouple>> corpus, String elapsed_secs){        
        HashMap<String, Double> not_recognized = new HashMap<>(); 
        HashMap<String, Double> mismatched_couples = new HashMap<>();       
                
        DecimalFormat df = new DecimalFormat("#0.00%");
        df.setRoundingMode(RoundingMode.UP);

                
        int tag_corretti = 0;
        int parole_esaminate = 0;
        
        int error_counter = 0;
        
        
        if(tagger.size() != corpus.size()) throw new IllegalArgumentException("I due array di frasi di coppie parole-tag non hanno la stessa lunghezza (mancano delle frasi).");
             
        for(int i = 0; i < tagger.size(); i++) { //scorre le frasi
            if(tagger.get(i).size() != corpus.get(i).size()) throw new IllegalArgumentException("Le due frasi (coppie parole-tag) non hanno la stessa lunghezza (mancano delle parole).");            
            for(int j = 0; j < tagger.get(i).size(); j++) { //scorre le parole
				
                
                parole_esaminate++;                
                
                String from_tagger = tagger.get(i).get(j).getTag();
                String from_corpus = corpus.get(i).get(j).getTag();
				
                
                //Se il tagging è corretto
                if(from_tagger.equals(from_corpus))
                    tag_corretti++;
                else { //c'è stato un errore
                    error_counter++;
                    //System.out.println("errore su: "+tagger.get(i).get(j).getWord()+ "("+from_tagger+" invece di "+from_corpus+")");
                    
                    //un tag (del corpus) non è stato riconosciuto: lo conteggio
                    if(!not_recognized.containsKey(from_corpus)) 
                            not_recognized.put(from_corpus, 1.0);
                    else
                            not_recognized.put(from_corpus, not_recognized.get(from_corpus)+1);   
                    
                    
                    //Considero il mismatch fra tag (la coppia)
                    String couple = from_corpus+" - "+from_tagger;
                    if(!mismatched_couples.containsKey(couple))
                        mismatched_couples.put(couple, 1.0);
                    else
                        mismatched_couples.put(couple, mismatched_couples.get(couple)+1);        
                    
                }

                             
                    
            }
        }

        //normalizziamo per il numero di parole esaminate e prepariamo la stringa
        String not_recognized_string = "";
        for (Map.Entry<String, Double> entrySet : not_recognized.entrySet()) {
            String key = entrySet.getKey();
            Double value = entrySet.getValue();
            not_recognized.put(key, value/error_counter);
        }
        Map<String, Double> not_recognized_ordered = sortByValue(not_recognized);
        for (Map.Entry<String, Double> entrySet : not_recognized_ordered.entrySet()) {
            String key = entrySet.getKey();
            Double value = entrySet.getValue();
            not_recognized_string+=key+"\t--> "+df.format(value)+"\n";
        }   
        
        String mismatch_string = "";
        for (Map.Entry<String, Double> entrySet : mismatched_couples.entrySet()) {
            String key = entrySet.getKey();
            Double value = entrySet.getValue();
            mismatched_couples.put(key, value/error_counter);
        }
        Map<String, Double> mismatched_couples_ordered = sortByValue(mismatched_couples);
        for (Map.Entry<String, Double> entrySet : mismatched_couples_ordered.entrySet()) {
            String key = entrySet.getKey();
            Double value = entrySet.getValue();
            String onetabmore = "";
            if(key.length() <8 ) onetabmore = "\t"; 
            mismatch_string+=key+"\t"+onetabmore+"--> "+df.format(value)+"\n";
        }
        
        
        // @@@@ Preparazione della stringa da stampare @@@@
        String final_data = "\n### "+method_name+" ### \n\n\n"+
                "Seen words:\t"+parole_esaminate+
                "\nCorrect tags:\t"+tag_corretti+
                "\nTag accuracy:\t"+df.format(((double) tag_corretti/ (double) parole_esaminate))+
                "\nElapsed time:\t"+elapsed_secs+" secs\n\n\n"+
                "Statistic on not recognized tags:\n\n"+not_recognized_string+"\n\n"+
                "Statistic on mismatched tag couples (was in corpus - was tagged):\n\n"+mismatch_string+"\n\n";
                
        
        //Stampa su file
        printStringAsFile(pos_result_folder+method_name.replace(": ", "_")+".txt", final_data);
        
        //Stampa a video
        System.out.println(final_data);

    }

    
    /**
     * Ordina una Map per valore.
     * 
     * @param <K>
     * @param <V>
     * @param map
     * @return 
     */
    public static <K, V extends Comparable<? super V>> Map<K, V>  sortByValue( Map<K, V> map ) {
        Map<K,V> result = new LinkedHashMap<>();
        Stream <Entry<K,V>> st = map.entrySet().stream();
        
        st.sorted(Collections.reverseOrder(Comparator.comparing(e -> e.getValue())))
            .forEach(e ->result.put(e.getKey(),e.getValue()));

     return result;
    }
	
	
	/**
	 * Restituisce la stringa elaborata da evalB, chiamato da terminale.
	 * 
	 * @param cky_parse alberi ottenuti dal parse
	 * @param corpus alberi originali del test set
	 * @param POS_MODE_SUFFIX
	 */
	public static void printEvalB(String cky_parse, String corpus, String POS_MODE_SUFFIX) {
		String result = "";
		
		try {
			Process p = Runtime.getRuntime().exec("lib/EVALB/evalb -p lib/EVALB/sample/sample.prm "+corpus+" "+cky_parse);
			
			//System.out.println("Executing: lib/EVALB/evalb -p lib/EVALB/sample/sample.prm "+corpus+" "+cky_parse);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String one_line;
			
			// leggo il responso di evalb
			while ((one_line = in.readLine()) != null) {
				 result += one_line+"\n";
				 System.out.println(one_line);
			}
			System.out.println("\n");
			
		}catch(IOException | NumberFormatException e){
			e.printStackTrace();
		}
		
		//scrivi in un file nella cartella cky_result_folder
		printStringAsFile(cky_result_folder+"cky_evalb_results"+POS_MODE_SUFFIX+".txt", result);
	}
}

