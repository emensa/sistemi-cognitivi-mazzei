/*
 * Classe Singleton Mantiene un'istanza di Morphit.
 */
package NLPutil;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Enrico
 */
public class MorphitWrapper {
	
	private static MorphitWrapper instance = null;
	
	//Usati solo nel caso di lemmatizzazione con morphit
	private static final Map<String, String> Google_Morphit_POS = new HashMap<>();
	private static final ListMultimap<String, String[]> MORPHIT = ArrayListMultimap.create();
	private static final String morphit_path = "corpus/lemmer/morph-it_048.txt";
	
	public MorphitWrapper() {}
	
    public static  MorphitWrapper getInstance() {
        if (instance == null) {
            instance = new MorphitWrapper();
			load_morphit();
			
        }
        return instance;
    }
		
	/**
	 * 
	 * Inizializza le strutture necessarie per la lemmatizzazione.
	 * 
	 */
	public static void load_morphit() {

		Google_Morphit_POS.put("NOUN", "NOUN");
		Google_Morphit_POS.put("PNOUN", "NPR");			
		Google_Morphit_POS.put("ADJ", "ADJ");
		Google_Morphit_POS.put("VERB", "VER");
		Google_Morphit_POS.put("CONJ", "CON");
		Google_Morphit_POS.put("PRON", "PRO");
		Google_Morphit_POS.put("ADV", "ADV");
		Google_Morphit_POS.put("ADP", "PRE");
		Google_Morphit_POS.put("DET", "DET");
		Google_Morphit_POS.put(".", "PON");
		Google_Morphit_POS.put("X", "X");
		Google_Morphit_POS.put("NUM", "NUM");
		Google_Morphit_POS.put("AUX", "AUX");
		
		try { 
			BufferedReader brl = new BufferedReader(new InputStreamReader(new FileInputStream(morphit_path), "ISO-8859-1"));
                        
			String line = "";
			boolean trovato;
			//per ogni riga(definizione) in motphit
			while((line = brl.readLine()) != null) {
				trovato = false;
                                
				//splitto la riga, ottenendo <parola, lemma, pos>
				String [] splitted_line = line.split("\t");
				if(splitted_line.length==3) {
					String key=splitted_line[0];

					//ottengo una versione semplificata del pos (solo il tipo di parola, senza info di genere, ...)
					String morphit_pos = splitted_line[2];
					if(morphit_pos.contains(":"))
						morphit_pos = morphit_pos.substring(0, splitted_line[2].indexOf(":"));
                                        if(morphit_pos.contains("NUM"))
                                            morphit_pos = "NUM";
                                        else if(morphit_pos.contains("-"))
						morphit_pos = morphit_pos.substring(0, morphit_pos.indexOf("-"));
					String [] val = {splitted_line[1], morphit_pos};
					if(MORPHIT.containsKey(key)) {
						for(String [] existing_value : MORPHIT.get(key))
							if(existing_value[0].equals(splitted_line[1])) {
								trovato = true;
								break;
							}
					}
					if(!trovato)
						MORPHIT.put(key,  val);
				}
			}
			brl.close();
		} catch (Exception ex ){
			ex.printStackTrace();
		}
	}
	
	
	/**
	 * Calcola il valore di probabilità per una coppia parola-tag usando le statistiche di morphit.
	 * 
	 * @param tag
	 * @param word
	 * @param TAGSET_TYPE
	 * @param ntag numero totale di tag
	 * @return 
	 */
	public double smoothingViterbi(String tag, String word, String TAGSET_TYPE, double ntag) {
				
		HashMap<String, Double> tag_prob = new HashMap<>();
		int total_occ = 0;

		if(MORPHIT.containsKey(word)) {
			for(String[] entry : MORPHIT.get(word)) {		
				total_occ++;
				if(tag_prob.containsKey(entry[1]))
					tag_prob.put(entry[1], tag_prob.get(entry[1])+1);
				else
					tag_prob.put(entry[1], 1.0);
			}
			if(tag_prob.containsKey(Google_Morphit_POS.get(tag))) 
				return tag_prob.get(Google_Morphit_POS.get(tag))/total_occ;
			else 
				//return (1/(ntag-total_occ))*((ntag-((total_occ/total_occ+1)*ntag))/ntag);
                return 1/ntag;
			
		} else {//la parola non c'è in morphit
			
			//Verifichiamo finamente: è un numero?
			if(word.matches("(\\'-)?\\d+(\\.\\d+)?")) { //se la parola è un numero
				if(tag.equals("NUM"))
					return 1.0;
				else 
					return 0.0;
			}

			//se non è né un numero né un nome proprio, assegno probabilità
			//P(word|tag) = (ntag-1)/ntag se il tag scelto è NOUN, e distribuisce equalemente le probabilità sugli altri tag.
		
			if(TAGSET_TYPE.equals("C5")) {
				if(tag.equals("NOUN") || tag.equals("PNOUN")) 
					return ((ntag-1)/2)/ntag;
				else 
					return 1/(ntag*(ntag-1));
			} else {
				if(tag.equals("NOUN")) 
					return (ntag-1)/ntag;
				else 
					return 1/(ntag*(ntag-1));				
			}
		}
		
	}
	
	
	/**
	 * Fornisce una lista di coppie <Google_TAG, probabilità> 
	 * La lista contiene per ogni tag Google_TAG la probabilità che la parola word sia taggata con esso 
	 * 
	 * @param word 
	 * @return 
	 */
	 public ArrayList<String[]> smoothingCky(String word) {
		ArrayList<String[]> pair_list = new ArrayList<>();
		for (Map.Entry<String, String> entry : Google_Morphit_POS.entrySet()) {
			String tag = entry.getKey();
			if(!tag.equals("PNOUN") && !tag.equals("AUX")) {
				String[] pair = new String[3];
				pair[0] = tag;
				pair[1] = ""+smoothingViterbi(tag, word, "C4", Google_Morphit_POS.size()-2); //riuso del codice!

				pair_list.add(pair);
			}
		}
		return pair_list;
	 }


	/**
	 * Lemming by morphit.
	 * 
	 * @param word
	 * @return 
	 */
	public String lemming(String word) {
		String second_annotation = word;   

		if(MORPHIT.containsKey(word)) {
			second_annotation = MORPHIT.get(word).get(0)[0];				
			
		}
		//se sono arrivato qui o morphit non contiene la parola che cerchiamo
		//oppure non ho trovato un pos uguale a quello di google, 
		//ritorno quindi il secondo (e se non ho trovato proprio la parola sarà null)
		return second_annotation;
	}


}
