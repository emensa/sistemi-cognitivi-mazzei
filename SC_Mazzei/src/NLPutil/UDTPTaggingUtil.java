/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NLPutil;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Raccoglitore di metodi che estraggono strutture dati a partire dall'Universal Dependecy Treebank Project.
 * Sono presenti anche metodi per la normalizzazione adottati sia da Viterbi che da Baseline.
 * 
 * @author Enrico
 */
public class UDTPTaggingUtil {
	
	/**
     * ##############################
     * ##############################
     * 
     * Metodi per l'estrazione da file
     * 
     * ##############################
     * ##############################
     */
	
	
	
    /**
     * Legge un file del corpus del The Universal Dependecy Treebank Project 
     * ed etrae la tag list dalla quarta colonna.
     * 
     * Si suppone che tutti i tag appaiano almeno una volta.
     * 
     * @param file_path
     * @param TAGSET C4 oppure C5
     * @return
     * @throws IOException 
     */
    public static ArrayList<String>UDTP_ExtractTagList(String file_path, String TAGSET) throws IOException {
        
        ArrayList<String> tag_list = new ArrayList<>();     
        
        BufferedReader br = new BufferedReader(new FileReader(file_path));
        
        String tag;
        
        String line;
        while((line = br.readLine()) != null) {
            String [] splitted_line = line.split("\t");
            
            if(splitted_line.length > 1) { // non è una linea di separazione
                if(TAGSET.equals("C5")) //C5
                    tag = splitted_line[4];
                else if(TAGSET.equals("C4")) //C4
                    tag = splitted_line[3];
                else throw new IllegalArgumentException("Codice tagset non esistente.");
                
                if(!tag_list.contains(tag)) tag_list.add(tag);
                
            }
        }
        br.close();
        		
        return tag_list;
    }    

    /**
     * Legge un file del corpus del The Universal Dependecy Treebank Project 
     * ed estrae il conteggio delle associazioni fra la seconda colonna (parole) e la quarta (tag).
     * 
     * 
     * @param file_path
     * @param TAGSET_TYPE C4 o C5
	 * @param NORMALIZATION_TYPE
     * @return Una HashMap che associa ad ogni coppia parola-tag il count con le occorrenze rinvenute nel corpus.
     * @throws IOException 
     */
    public static HashMap<String, HashMap<String, Integer>>  UDTP_WordTagCount(String file_path, String TAGSET_TYPE, String NORMALIZATION_TYPE) throws IOException {
        
        // Estrazione delle stopword da
        BufferedReader br = new BufferedReader(new FileReader(file_path));
        
        String line;
        String tag;
        
        HashMap<String, HashMap<String, Integer>> word_tag_count = new HashMap<>();
        
        while((line = br.readLine()) != null) {
            String [] splitted_line = line.split("\t");
            
            if(splitted_line.length > 1) { // non è una linea di separazione
                if(TAGSET_TYPE.equals("C5")) //C5
                    tag = splitted_line[4];
                else if(TAGSET_TYPE.equals("C4")) //C4
                    tag = splitted_line[3];
                else throw new IllegalArgumentException("Codice tagset non esistente.");
				String parola = norm(splitted_line[1], NORMALIZATION_TYPE); //normalizzazione se necessario

                
                
                if(word_tag_count.containsKey(parola)) { //la parola è già nella struttura, aumento solo il count per il tag corretto.
                    if(word_tag_count.get(parola).containsKey(tag)) //anche il tag c'è già
                        word_tag_count.get(parola)
                                .put(tag, word_tag_count.get(parola).get(tag)+1); //incremento il conteggio per il tag
                    else { // non c'era il tag
                        word_tag_count.get(parola).put(tag, 1);
                    }
                } else { //non c'era neanche la parola
                    word_tag_count.put(parola, new HashMap<>()); //inizializzo la map
                    word_tag_count.get(parola).put(tag, 1); //e metto subito il tag
                }
                
            }
        }
        br.close();
        
        return word_tag_count;
    }
    
    /**
     * Legge un file del corpus del The Universal Dependecy Treebank Project 
     * e popola le due strutture transition_probability e emission_probability
     * che contengono le probabilità di transizione e di emissione.
     * 
     * Nella computazione viene introdotto un tag fittizio S0 al fine di calcolare 
     * correttamente la transition probability.
     * 
     * @param file_path
     * @param tag_list
	 * @param TAGSET_TYPE
     * @param NORMALIZATION_TYPE
     * @return Una lista che ha ha in posizione 0 la transition probability e in posizione 1 la emission probability.
     * @throws IOException 
     */
    public static ArrayList<HashMap<String, HashMap<String, Double>>>UDTP_TransitionEmissionProbability(String file_path, ArrayList<String> tag_list, String TAGSET_TYPE, String NORMALIZATION_TYPE) throws IOException {
        
        HashMap<String, HashMap<String, Double>> transition_probability = initHashMatrixTransition(tag_list);
        HashMap<String, HashMap<String, Double>> emission_probability = initHashMatrixEmission(tag_list);     
        
        //conteggio sui singoli tag (già inizializzato con tutti i tag possibili)
        HashMap<String, Double> tag_count = initHashMapCounters(tag_list);
         
        // Estrazione delle stopword da
        BufferedReader br = new BufferedReader(new FileReader(file_path));
        
        String line;
        
        
        //Variabili di loop
        String previous_tag = "S0"; //t_{i-1}
        String current_tag; //t_i

        //il tag count conteggia già un S0 che altrimenti sarebbe scluso
        tag_count.put("S0", (double) 1); //inizializzo la map
        
        while((line = br.readLine()) != null) {
            String [] splitted_line = line.split("\t");
            
            if(splitted_line.length > 1) { // non è una linea di separazione
				if(TAGSET_TYPE.equals("C5")) //C5
                    current_tag = splitted_line[4];
                else if(TAGSET_TYPE.equals("C4")) //C4
                    current_tag = splitted_line[3];
                else throw new IllegalArgumentException("Codice tagset non esistente.");
				String parola = norm(splitted_line[1], NORMALIZATION_TYPE); //normalizzazione se necessario
                
                
                // ########## OCCORRENZE dei tag  ########## 
                //Le occorrenze ci serviranno dopo per i denominatori delle probabilità (sia di emissione che di transizione)
                tag_count.put(current_tag, tag_count.get(current_tag)+1);
                
                
                // ########## Probabilità di TRANSIZIONE ########## 
                // Incremento il count della cella <t_{i-1}, t_i> 
                transition_probability.get(previous_tag).put(current_tag, transition_probability.get(previous_tag).get(current_tag)+1); //aggiorno easy

                // ########## Probabilità di EMISSIONE ########## 
                // Incremento il count della cella <t_i, w_i> (verificando che la parola ci sia)
                if(emission_probability.get(current_tag).containsKey(parola)) { //esiste w_i
                    emission_probability.get(current_tag)
                            .put(parola, emission_probability.get(current_tag).get(parola)+1); //aggiorno easy
                } else { //non ho mai incontrato w_i, la aggiungo a tutti i tag
                    for (String tag : tag_list) {
                        if(!emission_probability.get(tag).containsKey(parola)) { //introduco w_i in ogni hashmap che regola i map
                            if(tag.equals(current_tag)) emission_probability.get(current_tag).put(parola, (double) 1); // se sono uguali, l'ho appena trovata, quindi count ad 1
                            else emission_probability.get(tag).put(parola, (double) 0); //altri casi, count a 0
                        }
                    }
                }         
                
            } else { //era una linea di separazione, non dobbiamo fare niente in transition_probability né emission_probability
                //dobbiamo però aumentare degli S0
                tag_count.put("S0", tag_count.get("S0")+1); //inizializzo la map
                current_tag = "S0";
            }
        
            previous_tag = current_tag; //aggiorno il previous

        } //fine while
        
        br.close();

        //Completo l'opera dividendo per il count come richiesto dalla formula.
        
        //Transition
        for (HashMap.Entry<String, HashMap<String, Double>> entry : transition_probability.entrySet()) { //associazione fra un tag e tutti gli altri tag (una riga, insomma) 
            String prev_tag = entry.getKey(); //t_{i-1}
            HashMap<String, Double> tag_i_hmap = entry.getValue(); //hash map con tutti gli i-esimi
                  
            for (HashMap.Entry<String, Double> entry_2 : tag_i_hmap.entrySet()) { //associazione singola fra due tag (prev e current)
                String curr_tag = entry_2.getKey(); //t_i
                Double curr_tag_count = entry_2.getValue(); //C(t_i, t_{i-1})

                //Calcolo: P(t_i| t_{i-1}) = C(t_i, t_{i-1})/C(t_{i-1})
                tag_i_hmap.put(curr_tag, curr_tag_count/tag_count.get(prev_tag)); //divido per il count
                
            }

        }
        
        //Emissione
        for (HashMap.Entry<String, HashMap<String, Double>> entry : emission_probability.entrySet()) { //associazione fra un tag e tutte le sue parole (una riga, insomma) 
            String curr_tag = entry.getKey(); //t_i
            HashMap<String, Double> parole_hmap = entry.getValue(); //hash map con tutte le parole
                  
            for (HashMap.Entry<String, Double> entry_2 : parole_hmap.entrySet()) { //associazione fra tag (current) e parola
                String parola = entry_2.getKey(); //w_i
                Double parola_count = entry_2.getValue(); //C(t_i, w_i})

               
                //Calcolo: P(w_i| t_i) = C(t_i, w_i)/C(t_i)
                parole_hmap.put(parola, parola_count/tag_count.get(curr_tag)); //divido per il count
                
            }

        }
        
        //Preparo il risultato male
        ArrayList<HashMap<String, HashMap<String, Double>>>  result = new ArrayList<>();
        
        result.add(transition_probability);
        result.add(emission_probability);
        
        //una porcata che fa risparmiare computazione... la lista dei tag la prendo da qui
        HashMap<String, HashMap<String, Double>> container_tag_list = new HashMap<>();
        container_tag_list.put("tag_list", tag_count);
        result.add(container_tag_list);

        
        return result;
    }    
    
        
     /**
     * Legge il un file del corpus del The Universal Dependecy Treebank Project 
     * ed estrae la seconda e quarta colonna associandole in un oggetto di tipo WordTagCouple.
     * 
     * Metodo utilizzato per poter estrarre dal training set.
     * 
     * @param file_path
     * @param TAGSET_TYPE C4 o C5
	 * @param NORMALIZATION_TYPE
     * @return Una lista di liste WordTagCouple che contempla tutto il corpus. Ogni lista interna rappresenta una frase (così è come se considerassimo S0).
     * @throws IOException 
     */
    public static ArrayList<ArrayList<WordTagCouple>> UDTP_WordTagCouples(String file_path, String TAGSET_TYPE, String NORMALIZATION_TYPE) throws IOException {
        
        ArrayList<ArrayList<WordTagCouple>> result_corpus = new ArrayList<>();
        
        ArrayList<WordTagCouple> wtc = new ArrayList<>();
        
        // Estrazione delle stopword da
        BufferedReader br = new BufferedReader(new FileReader(file_path));
        
        String line;
        String tag;
                
        while((line = br.readLine()) != null) {
            String [] splitted_line = line.split("\t");
            
            if(splitted_line.length > 1) { // non è una linea di separazione
                if(TAGSET_TYPE.equals("C5")) //C5
                    tag = splitted_line[4];
                else if(TAGSET_TYPE.equals("C4")) //C4
                    tag = splitted_line[3];
                else throw new IllegalArgumentException("Codice tagset non esistente.");
				
				String parola = norm(splitted_line[1], NORMALIZATION_TYPE); //normalizzazione se necessario
                
                WordTagCouple one_wtc = new WordTagCouple(parola, tag);
                
                wtc.add(one_wtc);
                
            } else { //era una linea vuota, fine della parola (nuovo array)
                result_corpus.add(wtc);
                wtc = new ArrayList<>();
            }
        }
        br.close();
        
        return result_corpus;
    }
    
    
	/**
     * ##############################
     * ##############################
     * 
     * Metodi per la normalizzazione
     * 
     * ##############################
     * ##############################
     */
	
	/**
	 * 
	 * Normalizza una parola.
	 * Qualora il lemma non si trovi, 
	 * 
	 * @param word
	 * @param NORMALIZATION_TYPE
	 * @return 
	 */
	public static String norm(String word, String NORMALIZATION_TYPE) {
		
	    switch(NORMALIZATION_TYPE) {                    
			case "NONE" : 
				return word;

			case "CAPITALIZE" : 
				return word.toLowerCase();
				
			case "LEMMING" : 
				return MorphitWrapper.getInstance().lemming(word);					

			default: 
				throw new IllegalArgumentException("Normalization type inesistente.");
		}
	}
	

		
    
	/**
     * #################
     * #################
     * 
     * Metodi di utilità
     * 
     * #################
     * #################
     */

    
    /**
     * Semplice metodo di inizializzazione che popola la HashMap con tutte le chiavi fornite in input e valore 0.
     * @param keys
     * @return 
     */
    public static HashMap<String, Double> initHashMapCounters(ArrayList<String> keys) {
        HashMap<String, Double> hm = new HashMap<>();
        for (String key : keys) {
            hm.put(key, (double) 0);
        }
       return hm;
    }
    
    /**
     * Semplice metodo di inizializzazione che popola la HashMap con tutte le chiavi fornite in input e valore una hashmap vuota.
     * @param keys
     * @return 
     */
    public static HashMap<String, HashMap<String, Double>> initHashMatrixEmission(ArrayList<String> keys) {
        HashMap<String, HashMap<String, Double>> hm = new HashMap<>();
        for (String key : keys) {
            hm.put(key, new HashMap<>());
        }
       return hm;
    }
    
    /**
     * Semplice metodo di inizializzazione che popola la HashMap con tutte le chiavi fornite in input
     * e un'hash map con tutte le chiavi e i valori a 0 (sono HM a due livelli!).
     * @param keys
     * @return 
     */
    public static HashMap<String, HashMap<String, Double>> initHashMatrixTransition(ArrayList<String> keys) {
      HashMap<String, HashMap<String, Double>> hm = initHashMatrixEmission(keys);
        hm.put("S0", initHashMapCounters(keys));
        
        for (Map.Entry<String, HashMap<String, Double>> entrySet : hm.entrySet()) {
            String key = entrySet.getKey();
            
            hm.put(key, initHashMapCounters(keys));
        }
       return hm;
    }
           
}